<?php
use Illuminate\Database\Seeder;

class advertisement_master extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisement_master')->insert([
       'userId' => '5',
       'title' => 'Loofre Platinum Membership',
	   'url' => 'https://www.loofre.com/app/',
       'status' =>'1'
   ]);
    }
}
