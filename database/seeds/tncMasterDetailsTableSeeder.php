<?php
use Illuminate\Database\Seeder;
class tncMasterDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tnc_master_details')->insert([
       'nameTypeKey' => 'spa',
       'tncValue' => '<h3>Terms And Conditions</h3>',
       'status' =>'1'
   ]);
    }
}
