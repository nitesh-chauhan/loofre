<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_master', function (Blueprint $table) {
            $table->increments('id');
			$table->rememberToken();
			$table->integer('userId');
            $table->integer('productId');
			$table->integer('quantity');
			$table->float('price');
			$table->float('total');
			$table->date('createdDate');
			$table->char('orderId', 250);
			$table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_master');
    }
}
