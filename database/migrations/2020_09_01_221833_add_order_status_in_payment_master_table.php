<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderStatusInPaymentMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_master', function (Blueprint $table) {
            $table->enum('order_status', ['pending', 'cancel', 'reject', 'confirm', 'complete'])->default('pending')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_master', function (Blueprint $table) {
            $table->dropColumn('order_status');
        });
    }
}
