<?php
namespace App\Http\Middleware;
use Closure;
class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {
    	$domains = ['https://www.loofre.co.in', 'https://www.loofre.com']; 
        if(isset($request->server()['HTTPS_ORIGIN'])){
            $origin = $request->server()['HTTPS_ORIGIN'];
            if(in_array($origin, $domains)){
                header('Access-Control-Allow-Origin: '.  $origin);
                header('Access-Control-Allow-Headers: Origin, Content-Type');
            }
        }
        return $next($request);
            // ->header('Access-Control-Allow-Origin', '*')
            // ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}
