<?php 
namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Auth;
use App\User;
use Entrust;
use Session;
use Redirect;
class AuthController extends Controller
{
    /*Login Page*/
	
	public function empLogin(Request $request){
		if($request->isMethod('POST')){
			$this->validate($request,[
                'email' => 'required',
                'password' => 'required'
            ]);
            $email = $request->get('email');
            $password = $request->get('password');
            if (Auth::attempt(['email' => $email, 'password' => $password])){
                if(Auth::user()->status == 0){
                    Auth::logout();
                    Session::flash('flash_message', 'User Deactivated, Please contact to Administrator!');
                    return view('login');
                }elseif(Auth::user()->role_id == 7){
                        return redirect()->intended('/offer-dashboard');
                    }
				elseif(Auth::user()->role_id == 3){
      					return redirect('/dashboard');                      
                    }
					elseif(Auth::user()->role_id == 5){
						return redirect('/alliance-dashboard');
					}
            }else{
                Session::flash('flash_message', 'Email or Password is Incorrect');
                return Redirect()->back();
            }
		}else{
			return view('login');
		}
	}
}
