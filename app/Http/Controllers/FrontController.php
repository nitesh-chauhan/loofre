<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
class FrontController extends Controller
{
    public function index(Request $request)
    { 
      return view('FrontWebPage.index');
	   
    }
	public function spa(Request $request)
    { 
        return view('FrontWebPage.spa');
    }
}
 