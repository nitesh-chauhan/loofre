<?php
namespace App\Http\Controllers;

use App\Http\Kernel;
use App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use function PHPSTORM_META\elementType;
use App\Http\Controllers\Controller;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\Collection;
use Validator, Session, Redirect, Auth, Mail, Excel, Table;
use Illuminate\Support\Facades\{DB, Hash, URL, View, Input, Storage};
use App\{User, productMaster, categoryMaster, merchantStockMaster, cartMaster, menuMaster, paymentMaster};

class ApiController extends Controller
{
    // app user login   veg
    public function productListInfo(Request $request)
    {
        $productMaster = new productMaster;
        $menuId = $request->get('menuId');
        $category = $request->get('category');
        $productInfo = productMaster::where([['menu_id', '=', $menuId], ['category', '=', $category], ['status', '=', 1]])->get();
        $productInfoObj = (object)array(
            'productInfo' => $productInfo,
        );
        return response()->json($productInfoObj);

    }

    public function menuList(Request $request)
    {
        $menuMaster = new menuMaster;
        $categoryId = $request->get('categoryId');
        $menuMasterObj = menuMaster::where([['lo_menu_master.categoryId', '=', $categoryId], ['lo_menu_master.status', '=', 'Active']])->get();
        $menuMasterObj = (object)array(
            'menuMaster' => $menuMasterObj,
        );
        return response()->json($menuMasterObj);

    }

    public function merchantUnit(Request $request)
    {

        $category = categoryMaster::where('status', '=', 2)->get();
        if (!empty($category))
        {
            $categoryObj = (object)array(
                'category' => $category,
            );
            return response()->json($categoryObj);
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid categories!']);
        }
    }

    public function outletCategories(Request $request)
    {
        $category = categoryMaster::where('status', '=', 1)->orderByRaw(DB::raw("FIELD(categoryId, 1, 2) DESC"))
            ->get();
        if (!empty($category))
        {
            $categoryObj = (object)array(
                'category' => $category,
            );
            return response()->json($categoryObj);
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid categories!']);
        }
    }

    public function productCategories(Request $request)
    {
        $categories = categoryMaster::where('status', '=', 1)->get();

        return response()->json($categories);
    }

    public function unitCategories(Request $request)
    {
        $categories = categoryMaster::where('status', '=', 2)->get();

        return response()->json($categories);
    }

    public function sinupCustomer(Request $request)
    {
        $user = new User;
        $merchantCode = $request->get('merchantCode');
        $UserList = User::where('mobileNumber', $merchantCode)->where('role_id', 7)->first();

        if (!empty($UserList))
        {
            $response = array();

            $validator = Validator::make($request->all(), [
                'name'         => 'required',
                'mobileNumber' => 'required|numeric|min:10|digits:10|unique:users,mobileNumber',
                'password'     => 'required',
                'merchantCode' => 'required'
            ]);

            if ($validator->fails())
            {
                $response['status'] = 'error';
                $data = $validator->errors()
                    ->all();
                $response['msg'] = $data[0];
                return response()->json($response);

            }
            else
            {
                $user = new User;
                $user->name           = ucwords($request->get('name'));
                $user->email          = '';
                $user->mobileNumber   = $request->get('mobileNumber');
                $user->location       = '';
                $user->password       = bcrypt($request->get('password'));
                $user->remember_token = bcrypt($request->get('password'));
                $user->gender         = '';
                $user->address        = $request->get('address');
                $user->latitude       = '';
                $user->longitude      = '';
                $user->merchantCode   = $request->get('merchantCode');
                $user->role_id        = 6;
                $user->status         = 1;
                $user->created_at     = date("Y-m-d H:i:s");
                $user->updated_at     = date("Y-m-d H:i:s");
                $user->save();

                $lastId = $user->id;

                return response()->json(['status' => 'success', 'msg' => 'User Info Successfully Registered', 'authId' => $lastId, 'alldata' => $request->all() ]);
            }
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'Merchant Code Not Exist!']);
        }
    }

    public function signUp(Request $request)
    {
        $response  = array();

        $validator = Validator::make($request->all() , [
            'name'           => 'required',
            'password'       => 'required',
            'mobileNumber'   => 'required|numeric|min:10|digits:10|unique:users,mobileNumber',
            'deliveryCharge' => 'required',
            'category_id'    => 'required',
            'asm_code'       => 'sometimes|nullable|exists:asm_codes,code',
            'opening_hour'   => 'required|date_format:H:i',
            'closing_hour'   => 'required|date_format:H:i|after:opening_hour',
            'address'        => 'required'
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else
        {
            $user = new User;

            $user->name           = ucwords($request->name);
            $user->email          = '';
            $user->mobileNumber   = $request->mobileNumber;
            $user->location       = '';
            $user->password       = bcrypt($request->password);
            $user->remember_token = Hash::make($request->password);
            $user->gender         = '';
            $user->address        = $request->address;
            $user->latitude       = '';
            $user->longitude      = $request->deliveryCharge;
            $user->merchantCode   = '';
            $user->role_id        = 7;
            $user->asm_code       = ($request->has('asm_code')) ? $request->asm_code : NULL;
            $user->status         = 1;
            $user->opening_hour   = $request->opening_hour;
            $user->closing_hour   = $request->closing_hour;

            if ($user->save())
            {
                $lastId = $user->id;

                // automated add storck for any merchant
                $productObject = productMaster::where('authId', 1)->get()->toArray();

                foreach ($productObject as $item)
                {
                    if ( in_array($item['category'], $request->category_id) )
                    {
                        $newshowdata  = array(
                            'authId'       => $lastId,
                            'menu_id'      => $item['menu_id'],
                            'category'     => $item['category'],
                            'productName'  => $item['productName'],
                            'productImage' => $item['productImage'],
                            'splaceImage'  => $item['splaceImage'],
                            'price'        => $item['price'],
                            'unitType'     => $item['unitType'],
                            'offerPrice'   => $item['offerPrice'],
                            'about'        => $item['about'],
                            'description'  => $item['description'],
                            'status'       => 2,
                        );

                        productMaster::insert($newshowdata);
                    }
                }

                $user->categories()->attach($request->category_id);

                return response()->json([
                    'status'  => 'success',
                    'msg'     => 'You have successfully registered',
                    'authId'  => $lastId,
                    'alldata' => $user
                ]);
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'User Invalid']);
            }
        }
    }

    public function login(Request $request)
    {

        $response = array();
        $validator = Validator::make($request->all() , ['mobileNumber' => 'required|numeric|min:10', 'password' => 'required', ]);
        if ($validator->fails())
        {
            $response['status'] = 'error';
            $data = $validator->errors()
                ->all();
            $response['msg'] = $data[0];
            return response()->json($response);
        }
        else
        {
            $mobileNumber = $request->get('mobileNumber');
            $password = $request->get('password');
            if (Auth::attempt(['mobileNumber' => $mobileNumber, 'password' => $password]))
            {
                if (Auth::user()->status == 0)
                {
                    return response()
                        ->json(['status' => 'error', 'msg' => 'user Deactivate!']);
                }
                else
                {
                    $UserList = User::where('mobileNumber', $mobileNumber)->where('status', '1')
                        ->first();
                    $idUser = $UserList->id;
                    $userName = $UserList->name;
                    $userEmail = $UserList->email;
                    $mobileNumber = $UserList->mobileNumber;
                    $address = $UserList->address;
                    $merchantCode = $UserList->merchantCode;
                    $role_id = $UserList->role_id;
                    //get delivery Charge
                    $UserListObject = User::where('mobileNumber', $merchantCode)->where('status', '1')
                        ->first();
                    $longitude = $UserListObject->longitude;
                    return response()
                        ->json(['status' => 'success', 'idUser' => $idUser, 'role_id' => $role_id, 'name' => $userName, 'email' => $userEmail, 'mobileNumber' => $mobileNumber, 'merchantCode' => $merchantCode, 'address' => $address, 'deliveryCharge' => $longitude, 'msg' => 'User Logine!']);
                }
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'Mobile Number or Password Invalid!']);
            }

        }
    }

    public function merchantInfo(Request $request)
    {
        $merchantCode = $request->merchantCode;

        $user = User::with('categories')->where('mobileNumber', $merchantCode)
            ->where('status', '1')
            ->first();

        if ( !is_null($user)) {
            return response()->json($user);
        }

        return response()->json(['status' => 'error', 'msg' => 'Merchant Not Found.']);
    }

    public function profileUpdate(Request $request)
    {
        $response = array();

        $validator = Validator::make($request->all(), [
            'userId'       => 'required',
            'name'         => 'required',
            'email'        => 'unique:users,email,' . $request->userId,
            'mobileNumber' => 'required|unique:users,mobileNumber,' . $request->userId,
            'gender'       => 'required',
            'address'      => 'required',
            'opening_hour' => 'sometimes|required|date_format:H:i',
            'closing_hour' => 'sometimes|required|date_format:H:i|after:opening_hour',
            'category_id'  => 'sometimes|required|exists:category_master,categoryId',
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();
            $response['error'] = $data[0];

            return response()->json($response);
        }
        else
        {
            $userId = $request->userId;
            $user   = User::find($userId);

            $oldMobile = $user->mobileNumber;
            $newMobile = $request->mobileNumber;

            $profileDataUpdate = array(
                'name'         => $request->get('name'),
                'email'        => $request->get('email'),
                'mobileNumber' => $request->get('mobileNumber'),
                'gender'       => $request->get('gender'),
                'address'      => $request->get('address')
            );

            if ($user->role_id == 7) {
                if ( $request->has('opening_hour') ) {
                    $profileDataUpdate['opening_hour'] = $request->opening_hour;
                }

                if ( $request->has('closing_hour') ) {
                    $profileDataUpdate['closing_hour'] = $request->closing_hour;
                }
            }

            $user->update($profileDataUpdate);

            # Update merchant categories
            if ($request->has('category_id') && $user->role_id == 7) {

                $existingIds = $user->categories->map(function($item) {
                    return $item->categoryId;
                })->toArray();

                $updateIds = array_diff($request->category_id, $existingIds);
                $deleteIds = array_diff($existingIds, $request->category_id);

                $productObject = productMaster::where('authId', 1)->get()->toArray();

                foreach ($productObject as $item)
                {
                    if ( in_array($item['category'], $updateIds) )
                    {
                        $newshowdata  = array(
                            'authId'       => $userId,
                            'menu_id'      => $item['menu_id'],
                            'category'     => $item['category'],
                            'productName'  => $item['productName'],
                            'productImage' => $item['productImage'],
                            'splaceImage'  => $item['splaceImage'],
                            'price'        => $item['price'],
                            'unitType'     => $item['unitType'],
                            'offerPrice'   => $item['offerPrice'],
                            'about'        => $item['about'],
                            'description'  => $item['description'],
                            'status'       => 2,
                        );

                        productMaster::insert($newshowdata);
                    }
                }

                productMaster::where('authId', $userId)->whereIn('category', $deleteIds)->delete();
                $user->categories()->sync($request->category_id);
            }

            # If Merchant phone number updated, update customer's merchantCode
            if ($oldMobile !== $newMobile && $user->role_id == 7) {
                User::where('merchantCode', $oldMobile)->update(['merchantCode' => $newMobile]);
            }

            return response()->json(['status' => 'success', 'msg' => 'Profile Successfully Update!']);
        }
    }

    public function merchantStockSave(Request $request)
    {
        $response = [];

        $validator = Validator::make( $request->all(), [
            'productId'     => 'required|exists:product_master,id',
            'categoryId'    => 'required|exists:category_master,categoryId',
            'stockQuantity' => 'required',
            'price'         => 'required',
            'offerPrice'    => 'required',
        ]);

        if ( $validator->fails() ) {
            $data = $validator->errors()->all();
            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else {
            $product  = productMaster::find($request->productId);
            $unitType = categoryMaster::find($request->categoryId)->categoryName;

            $product->update([
                'price'         => $request->price,
                'offerPrice'    => $request->offerPrice,
                'stockQuantity' => $request->stockQuantity,
                'unitType'      => $unitType,
            ]);

            return response()->json(['status' => 'success', 'msg' => 'Stock Successfully Registered!']);
        }
    }

    public function productListCustomer(Request $request)
    {
        $merchantCode  = $request->merchantCode;
        $category      = $request->category;

        $UserObject = User::where([['mobileNumber', '=', $merchantCode], ['role_id', '=', 7]])->first();
        $merchantId = $UserObject->id;

        $where = [
            ['authId', '=', $merchantId],
            ['category', '=', $category],
            ['offerPrice', '!=', 0],
            ['status', '=', 2],
            ['stockQuantity', '!=', 0],
        ];

        if ($request->has('menuId')) {
            $where['menu_id'] = $request->menuId;
        }

        $merchantStockCustomer = productMaster::where($where)->get();

        $productListCustomerObj = (object)array(
            'merchantStockCustomer' => $merchantStockCustomer,
        );

        return response()->json($productListCustomerObj);
    }

    public function quantatyUpdateCustomer(Request $request)
    {
        $productMaster = new productMaster;
        $productId = $request->get('productId');
        $quantity = $request->get('quantity');
        $dataUpdate = array(
            'description' => $request->get('quantity') ,
            'created_at' => date("Y-m-d H:i:s") ,
            'updated_at' => date("Y-m-d H:i:s")
        );
        $productMaster->where('id', $productId)->update($dataUpdate);
        return response()->json(['status' => 'success', 'msg' => 'Quantity Successfully Update!']);
    }

    public function merchantProductList(Request $request)
    {
        $authId   = $request->authId;
        $category = $request->category;

        $where = [
            ['authId', '=', $authId],
            ['category', '=', $category]
        ];

        if ($request->has('menuId')) {
            $where['menu_id'] = $request->menuId;
        }

        $merchantStockData = productMaster::where($where)->get();

        $unique = $merchantStockData->unique('productName');
        $unique->values()->all();

        $merchantStockDataObj = (object)array(
            'merchantStockData' => $unique,
        );

        return response()->json($merchantStockDataObj);
    }

    public function merchantProductStatus(Request $request)
    {
        $productMaster = new productMaster;
        $productId = $request->get('productId');
        $productObjectList = productMaster::where('id', $productId)->first();
        $statusDB = $productObjectList->status;
        if ($statusDB == 0)
        {
            $updateProductStatus = array(
                'status' => 2,
                'created_at' => date('Y-m-d h:i:s') ,
                'updated_at' => date('Y-m-d h:i:s')
            );
            productMaster::where('id', $productId)->update($updateProductStatus);
            return response()->json(['status' => 'success', 'msg' => 'Product List Successfully Active!']);
        }
        else
        {
            $updateProductStatus = array(
                'status' => 0,
                'created_at' => date('Y-m-d h:i:s') ,
                'updated_at' => date('Y-m-d h:i:s')
            );
            productMaster::where('id', $productId)->update($updateProductStatus);
            return response()->json(['status' => 'success', 'msg' => 'Product List Successfully  Deactivate!']);
        }
    }

    public function addCart(Request $request)
    {
        $response = [];

        $validator = Validator::make( $request->all(), [
            'userId'       => 'required|exists:users,id',
            'merchantCode' => 'required|exists:users,mobileNumber,role_id,7',
            'productId'    => 'required|exists:product_master,id',
            'quantity'     => 'required|min:1',
            'price'        => 'required',
            'total'        => 'required',
        ]);

        if ( $validator->fails() ) {
            $response['status'] = 'error';
            $data = $validator->errors()->all();
            $response['msg'] = $data[0];

            return response()->json($response);
        }
        else {
            $userId          = $request->get('userId');
            $productId       = $request->get('productId');
            $merchantCode    = $request->get('merchantCode');
            $quantityRequest = $request->get('quantity');
            $price           = $request->get('price');
            $total           = $request->get('total');

            $UserList = User::where('mobileNumber', $merchantCode)->where('role_id', 7)->first();

            $merchantId = $UserList->id;

            $productMasterList = productMaster::where([['id', '=', $productId], ['authId', '=', $merchantId], ])->first();

            $stockQuantityDB = $productMasterList->stockQuantity;
            $unitType = $productMasterList->unitType;

            if ($quantityRequest <= $stockQuantityDB)
            {
                $cartMasterObje = cartMaster::where([
                    ['userId', '=', $userId],
                    ['productId', '=', $productId],
                    ['status', '=', 1],
                    ['orderId', '=', ''],
                ])->first();

                if (empty($cartMasterObje))
                {
                    $cartMaster                 = new cartMaster;
                    $cartMaster->remember_token = bcrypt($request->get('userId'));
                    $cartMaster->userId         = $request->get('userId');
                    $cartMaster->productId      = $request->get('productId');
                    $cartMaster->quantity       = $request->get('quantity');
                    $cartMaster->price          = $request->get('price');
                    $cartMaster->unitType       = $unitType;
                    $cartMaster->total          = $request->get('total');
                    $cartMaster->createdDate    = date("Y-m-d");
                    $cartMaster->orderId        = '';
                    $cartMaster->status         = 1;
                    $cartMaster->save();

                    return response()->json(['status' => 'success', 'msg' => 'cart successfully added!']);
                }
                else
                {
                    $quantityRequest = $cartMasterObje->quantity + $quantityRequest;

                    if ($quantityRequest <= $stockQuantityDB)
                    {
                        $cartIdDB = $cartMasterObje->id;

                        $updateData = array(
                            'quantity' => $quantityRequest,
                            'price' => $price,
                            'total' => $total,                    );

                        cartMaster::where('id', $cartIdDB)->update($updateData);
                        return response()->json(['status' => 'success', 'msg' => 'cart Update!']);
                    }
                    else {
                        return response()->json(['status' => 'error', 'msg' => 'No more Product in the Stock.']);
                    }
                }
            }
            else
            {
                return response()->json(['status' => 'error', 'msg' => 'No more Product in the Stock.']);
            }
        }
    }

    public function cartList(Request $request)
    {
        $authId = $request->get('authId');

        $cartDataList = cartMaster::leftJoin('product_master', 'cart_master.productId', '=', 'product_master.id')
            ->select('cart_master.id', 'cart_master.quantity', 'cart_master.productId', 'cart_master.price', 'cart_master.total',
                    'product_master.productName', 'product_master.productImage', 'product_master.unitType')
            ->where([
                ['cart_master.status', '=', 1],
                ['cart_master.userId', '=', $authId],
                ['cart_master.quantity', '>', 0]
            ])->get();

        $cartDataListObj = (object)array(
            'cartDataList' => $cartDataList,
        );

        return response()->json($cartDataListObj);
    }

    public function cartDelete(Request $request)
    {
        $cartMaster = new cartMaster;
        $productMaster = new productMaster;
        $cartId = $request->get('cartId');
        $cartObjectList = cartMaster::where('id', $cartId)->first();
        $productIdDB = $cartObjectList->productId;
        $productQuantitay = array(
            'description' => 0,
            'created_at' => date("Y-m-d H:i:s") ,
            'updated_at' => date("Y-m-d H:i:s")
        );
        $productMaster->where('id', $productIdDB)->update($productQuantitay);
        $res = cartMaster::where('id', $cartId)->delete();
        return response()
            ->json(['status' => 'success', 'msg' => 'Cart Product Successfully Delete!']);

    }

    /* public function qutiUpdate($productIdDB, $quantityDB)
    {
    $productMaster = new productMaster;
    $productList = productMaster::where('id', $productIdDB)
    ->get();
    $stockQuantityDB=$productList->stockQuantity;
    $stockQuantityTot=$stockQuantityDB - $quantityDB;
    $stockUpdate = array(
    'stockQuantity' => $stockQuantityTot,
    'created_at' => date("Y-m-d H:i:s"),
    'updcated_at' => date("Y-m-d H:i:s")
    );
    $productMaster->where('id', $productIdDBm)->update($stockUpdate);
    }  */

    public function quantityDecrement($id, $quantity)
    {
        $product = productMaster::find($id);
        $product->decrement('stockQuantity', $quantity);
    }

    public function quantityIncrement($id, $quantity)
    {
        $product = productMaster::find($id);
        $product->increment('stockQuantity', $quantity);
    }

    public function placeOrder(Request $request)
    {
        $userId = $request->get('userId');
        $existingOrders = paymentMaster::where('userId', $userId)->whereIn('order_status', ['pending', 'confirm'])->get();

        if ( count($existingOrders) <= 2 ) {

            $cartMaster    = new cartMaster;
            $productMaster = new productMaster;
            $paymentMaster = new paymentMaster;
            $user          = new User;

            $orderId = time() . mt_rand() . $userId;

            $cartMasterObject = cartMaster::where([
                ['cart_master.status',  '=', 1],
                ['cart_master.userId',  '=', $userId],
                ['cart_master.orderId', '=', ''],
            ])
            ->get();

            foreach ($cartMasterObject as $item)
            {
                $productIdDB = $item->productId;
                $quantityDB  = $item->quantity;

                $item->update([
                    'orderId' => $orderId,
                    'status'  => 2,
                ]);

                $this->quantityDecrement($productIdDB, $quantityDB);
            }

            $paymentMaster->remember_token = Hash::make($request->get('userId'));
            $paymentMaster->userId         = $request->get('userId');
            $paymentMaster->venderId       = $request->get('venderId');
            $paymentMaster->orderId        = $orderId;
            $paymentMaster->txind          = $request->get('txind');
            $paymentMaster->offer          = $request->get('offer');
            $paymentMaster->description    = $request->get('description');
            $paymentMaster->grand_total    = $request->get('grand_total');
            $paymentMaster->orderDate      = date('Y-m-d');
            $paymentMaster->approvedBy     = 'Payyou';
            $paymentMaster->status         = 'success';
            $paymentMaster->created_at     = date("Y-m-d H:i:s");
            $paymentMaster->updated_at     = date("Y-m-d H:i:s");
            $paymentMaster->save();

            return response()->json(['status' => 'success', 'msg' => 'Order Placed. Check your order history']);
        }
        else {
            return response()->json(['status' => 'error', 'msg' => 'You have already 2 orders in process.']);
        }
    }

    public function placeOrderCash(Request $request)
    {
        $userId = $request->get('userId');
        $existingOrders = paymentMaster::where('userId', $userId)->whereIn('order_status', ['pending', 'confirm'])->get();

        if ( count($existingOrders) <= 2 ) {

            $cartMaster    = new cartMaster;
            $productMaster = new productMaster;
            $paymentMaster = new paymentMaster;
            $user          = new User;

            $orderId = time() . mt_rand() . $userId;

            $cartMasterObject = cartMaster::where([
                ['cart_master.status', '=', 1],
                ['cart_master.userId', '=', $userId],
                ['cart_master.orderId', '=', ''],
            ])
            ->get();

            foreach ($cartMasterObject as $item)
            {
                $productIdDB = $item->productId;
                $quantityDB  = $item->quantity;

                $item->update([
                    'orderId' => $orderId,
                    'status'  => 2,
                ]);

                $productMaster->where('id', $productIdDB)->update(['description' => 0]);

                $this->quantityDecrement($productIdDB, $quantityDB);
            }

            $paymentMaster->remember_token = Hash::make($request->get('userId'));
            $paymentMaster->userId         = $request->get('userId');
            $paymentMaster->venderId       = $request->get('venderId');
            $paymentMaster->orderId        = $orderId;
            $paymentMaster->txind          = $request->get('txind');
            $paymentMaster->offer          = $request->get('deliverStatus');
            $paymentMaster->description    = $request->get('modePayment');
            $paymentMaster->grand_total    = $request->get('grand_total');
            $paymentMaster->orderDate      = date('Y-m-d');
            $paymentMaster->approvedBy     = 'Payyou';
            $paymentMaster->status         = 'pending';
            $paymentMaster->created_at     = date("Y-m-d H:i:s");
            $paymentMaster->updated_at     = date("Y-m-d H:i:s");
            $paymentMaster->save();

            return response()
                ->json(['status' => 'success', 'msg' => 'Order Placed Check your order history']);
        }
        else {
            return response()->json(['status' => 'error', 'msg' => 'You have already 2 orders in process.']);
        }
    }

    public function placeOrderCancal(Request $request)
    {
        $cartMaster    = new cartMaster;
        $productMaster = new productMaster;
        $paymentMaster = new paymentMaster;
        $user          = new User;
        $userId        = $request->get('userId');

        $orderId = time() . mt_rand() . $userId;

        $cartMasterObject = cartMaster::where([
            ['cart_master.status', '=', 1],
            ['cart_master.userId', '=', $userId]
        ])->get()->toArray();

        foreach ($cartMasterObject as $item)
        {
            $productIdDB = $item['productId'];
            $quantityDB  = $item['quantity'];

            $cartUpdate = array(
                'orderId' => $orderId,
                'status' => 2,
            );

            $cartMaster->where('productId', $productIdDB)->update($cartUpdate);

            $this->quantityIncrement($productIdDB, $quantityDB);
        }

        $paymentMaster->remember_token = Hash::make($request->get('userId'));
        $paymentMaster->userId = $request->get('userId');
        $paymentMaster->venderId = $request->get('venderId');
        $paymentMaster->orderId = $orderId;
        $paymentMaster->txind = $request->get('txind');
        $paymentMaster->offer = $request->get('offer');
        $paymentMaster->description = $request->get('description');
        $paymentMaster->grand_total = $request->get('grand_total');
        $paymentMaster->orderDate = date('Y-m-d');
        $paymentMaster->approvedBy = 'Payyou';
        $paymentMaster->status = 'failure';
        $paymentMaster->created_at = date("Y-m-d H:i:s");
        $paymentMaster->updated_at = date("Y-m-d H:i:s");
        $paymentMaster->save();

        return response()
            ->json(['status' => 'error', 'msg' => 'Your Place Order hasbeen Cancal']);
    }

    public function detailsOrder(Request $request)
    {
        $orderId = $request->get('orderId');
        $merchantStockMaster = new merchantStockMaster;
        $cartMaster = new cartMaster;

        $orderDataList = cartMaster::leftJoin('product_master', 'cart_master.productId', '=', 'product_master.id')
            ->select('cart_master.id', 'cart_master.quantity', 'cart_master.productId', 'cart_master.price', 'cart_master.total',
                'product_master.productName', 'product_master.productImage', 'product_master.unitType')
            ->where([['cart_master.orderId', '=', $orderId], ])
            ->get();

        $groupBy = $orderDataList->groupBy('productId')
            ->map(function ($item, $key) {
                $first = $item[0];
                $first['quantity'] = $item->sum('quantity');

                return $first;
            })->toArray();

        $orderDataListObj = (object)array(
            'orderDataList' => array_values($groupBy),
        );

        return response()->json($orderDataListObj);
    }

    //veg*******************************************************
    public function customerOrderList(Request $request)
    {
        $user = User::find($request->userId);

        if ( !is_null($user) ) {
            return response()->json(['status' => 'success', 'msg' => $user->orders]);
        }

        return response()->json(['status' => 'error', 'msg' => 'User not found.']);
    }

    public function merchantOrderList(Request $request)
    {
        $paymentMaster = new paymentMaster;
        $user = new User;
        $mobileNumber = $request->get('mobileNumber');

        // $menrOrderList = paymentMaster::leftJoin('users', 'payment_master.venderId', '=', 'users.mobileNumber')
        //     ->where([['payment_master.venderId', '=', $mobileNumber], ])
        //     ->get();

        // $merDataListObj = (object)array(
        //     'merDataList' => $menrOrderList,
        // );

        $orders = paymentMaster::with('customer')->where('venderId', $mobileNumber)->get();
        return response()->json(['merDataList' => $orders]);
    }

    public function merchantCommunations(Request $request)
    {
        $category = categoryMaster::where('status', '=', 3)->get();
        if (!empty($category))
        {
            $categoryObj = (object)array(
                'category' => $category,
            );
            return response()->json($categoryObj);
        }
        else
        {
            return response()->json(['status' => 'error', 'msg' => 'invalid categories!']);
        }
    }

    public function merchantCustomerCommunations(Request $request)
    {
        $paymentMaster = new paymentMaster;
        $orderId = $request->get('orderId');
        $massage = $request->get('massage');
        $massageUpdate = array(
            'massage' => $massage,
            'created_at' => date("Y-m-d H:i:s") ,
            'updated_at' => date("Y-m-d H:i:s")
        );

        $paymentMaster->where('id', $orderId)->update($massageUpdate);

        return response()->json(['status' => 'success', 'msg' => 'Message will be Send']);
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;

        $user = User::find($request->authId);

        if ( !empty($user) ) {

            if ($user->role_id != 7) {
                $user = User::where('mobileNumber', $user->merchantCode)->first();
            }

            $id = $user->id;

            $products = productMaster::where('authId', $id)
                            ->where('productName', 'like', "%$keyword%")
                            ->get();

            return response()->json($products);
        }

        return response()->json(['status' => 'error', 'msg' => 'Merchant not found.']);
    }

    public function userCancelOrder(Request $request)
    {
        $response = [];

        $validator = Validator::make( $request->all(), [
            'userId'  => 'required|exists:users,id',
            'orderId' => 'required|exists:payment_master,orderId',
        ]);

        if ( $validator->fails() ) {
            $response['status'] = 'error';
            $data = $validator->errors()->all();
            $response['msg'] = $data[0];

            return response()->json($response);
        }
        else {
            $userId  = $request->get('userId');
            $orderId = $request->get('orderId');

            $payment = paymentMaster::where('userId', $userId)->where('orderId', $orderId)->first();

            if (!is_null($payment)) {

                if ($payment->order_status == 'cancel') {
                    return response()->json(['status' => 'success', 'msg' => 'Your order has already cancelled.']);
                }
                else {
                    $payment->update(['order_status' => 'cancel']);

                    $cart = cartMaster::where('userId', $userId)->where('orderId', $orderId)->get();

                    foreach ($cart as $item) {
                        $item->update(['status' => 2]);

                        $this->quantityIncrement($item->productId, $item->quantity);
                    }

                    return response()->json(['status' => 'success', 'msg' => 'Your order has been cancelled.']);
                }
            }

        }
    }

    public function orderTiming(Request $request)
    {
        $response = [];

        $validator = Validator::make( $request->all(), [
            'minute'  => 'required_if:status,confirm',
            'orderId' => 'required|exists:payment_master,orderId',
            'status'  => 'required|in:confirm,reject,deliver,not_deliver'
        ]);

        if ( $validator->fails() ) {
            $response['status'] = 'error';
            $data = $validator->errors()->all();
            $response['msg'] = $data[0];

            return response()->json($response);
        }
        else {
            $message = $request->input('minute', NULL);
            $orderId = $request->orderId;
            $status  = $request->status;

            $payment = paymentMaster::where('orderId', $orderId)->first();
            $payment->update(['massage' => $message, 'order_status' => $status]);

            return response()->json(['status' => 'success', 'msg' => "Order updated."]);
        }
    }

    public function orderMessage(Request $request)
    {
        $orderId = $request->get('orderId');

        $payment = paymentMaster::where('orderId', $orderId)->first();

        if ( !is_null($payment) ) {

            $message = 'Your order is pending.';

            if ($payment->order_status == 'confirm') {
                $time = $this->convertToHoursMins($payment->massage);
                $message = "On the way. You will receive in " . $time;
            }
            elseif ($payment->order_status == 'reject') {
                $message = "Your order is rejected.";
            }
            elseif ($payment->order_status == 'cancel') {
                $message = "Your order is cancelled.";
            }
            elseif ($payment->order_status == 'deliver') {
                $message = "Your order is delivered.";
            }
            elseif ($payment->order_status == 'not_deliver') {
                $message = "Your order is not delivered.";
            }

            return response()->json([
                'status'       => 'success',
                'msg'          => $message,
                'order_status' => $payment->order_status
            ]);
        }
        else {
            return response()->json(['status' => 'error', 'msg' => 'invalid order!']);
        }
    }

    public function sendPasswordSms(Request $request)
    {
        $response = [];

        $validator = Validator::make( $request->all(), [
            'mobileNumber' => 'required|exists:users,mobileNumber',
        ]);

        if ( $validator->fails() ) {
            $response['status'] = 'error';
            $data = $validator->errors()->all();
            $response['msg'] = $data[0];

            return response()->json($response);
        }
        else {
            $user = User::where('mobileNumber', $request->get('mobileNumber'))->first();

            if ( !is_null($user) ) {
                $otp = mt_rand(100000, 999999);

                $user->update(['otp' => $otp]);

                $name = $user->name;
                $mobile = "91" . $user->mobileNumber;

                $msg = "Hi $name, your OTP is $otp";

                $path ='http://www.pushandpull.in/sendmsg.php?uname=K5Brand&pass=k)9So~1A&send=loofre&dest='.$mobile.'&msg='.urlencode($msg);

                $ch = curl_init($path);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $result = curl_exec($ch);
                curl_close($ch);

                return response()->json(['status' => 'success', 'msg' => 'We have sent you an SMS. Please check.']);
            }

            return response()->json(['status' => 'error', 'msg' => 'User not found.']);
        }
    }

    public function verifyOtp(Request $request)
    {
        $response = array();

        $validator = Validator::make($request->all(), [
            'otp'          => 'required',
            'mobileNumber' => 'required|exists:users,mobileNumber',
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else {
            $user = User::where('mobileNumber', $request->mobileNumber)
                        ->where('otp', '=', $request->otp)
                        ->first();

            $verified = ( !is_null($user) ) ? true : false;

            return response()->json(['status' => 'success', 'verified' => $verified]);
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'mobileNumber' => 'required|exists:users,mobileNumber',
            'otp'          => 'required|exists:users,otp',
            'password'     => 'required|string|min:6',
        ]);

        if ( $validator->fails() ) {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else {
            $user = User::where('mobileNumber', $request->mobileNumber)
                    ->where('otp', $request->otp)
                    ->first();

            if ( !is_null($user) ) {
                $user->update([
                    'otp'      => NULL,
                    'password' => bcrypt($request->password)
                ]);

                return response()->json(['status' => 'success', 'msg' => 'Password updated successfully.']);
            }

            return response()->json(['status' => 'error', 'msg' => 'User not found.']);
        }
    }

    public function resetPasswordByWeb(Request $request)
    {
        if (! $request->hasValidSignature()) {
            abort(401);
        }

        $validator = Validator::make( $request->all(), [
            'mobileNumber' => 'required|exists:users,mobileNumber',
            'otp' => 'required|exists:users,otp',
        ]);

        if ( $validator->fails() ) {
            $response['status'] = 'error';
            $data = $validator->errors()->all();
            $response['msg'] = $data[0];

            return response()->json($response);
        }
        else {
            $user = User::where('mobileNumber', $request->get('mobileNumber'))
                ->where('otp', $request->get('otp'))
                ->first();

            $user->update([
                'otp' => NULL,
                'password' => bcrypt($request->get('password'))
            ]);

            return response()->json(['status' => 'success', 'msg' => 'Password updated successfully.']);
        }
    }

    public function changeVendor(Request $request)
    {
        $response = array();

        $validator = Validator::make($request->all(), [
            'userId'       => 'required|exists:users,id',
            'merchantCode' => 'required|exists:users,mobileNumber',
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else
        {
            $user = User::find($request->userId);
            $user->update(['merchantCode' => $request->merchantCode]);

            return response()->json(['status' => 'success', 'msg' => 'Merchant changed successfully.']);
        }
    }

    public function customerCreateVendor(Request $request)
    {
        $response = array();

        $validator = Validator::make($request->all(), [
            'userId'       => 'required|exists:users,id',
            'mobileNumber' => 'required|exists:users,mobileNumber',
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else
        {
            $customer = User::find($request->userId);
            $merchant = User::where('mobileNumber', $request->mobileNumber)->first();

            $customer->merchants()->attach($merchant->id);

            return response()->json(['status' => 'success', 'msg' => 'Customer merchant added successfully.']);
        }
    }

    public function customerVendors(Request $request)
    {
        $response = array();

        $validator = Validator::make($request->all(), [
            'userId' => 'required|exists:users,id',
        ]);

        if ($validator->fails())
        {
            $data = $validator->errors()->all();

            $response['status'] = 'error';
            $response['msg']    = $data[0];

            return response()->json($response);
        }
        else
        {
            $customer = User::find($request->userId);
            $merchants = $customer->merchants;

            return response()->json($merchants);
        }
    }

    private function convertToHoursMins($time) {
        if ($time < 1) {
            return;
        }

        $hours = floor($time / 60);
        $minutes = ($time % 60);

        if (!$hours) {
            return sprintf('%02d minutes', $minutes);
        }

        return sprintf('%02d hours %02d minutes', $hours, $minutes);
    }
}

