<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class categoryMaster extends Model
{
    protected $table = 'category_master';

    protected $primaryKey = 'categoryId';

    public function categoryMaster() {
       return $this->hasOne('App\categoryMaster', 'categoryId');
    }

    public function users() {
	    return $this->belongsToMany('App\User');
	}
}
