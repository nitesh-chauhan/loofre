<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class cartMaster extends Model
{
    protected $table = 'cart_master';

    protected $fillable = ['status', 'orderId', 'unitType'];

	public function addcart(){
		 return $this->hasOne('App\cartMaster', 'id');
	}

}
