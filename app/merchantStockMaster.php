<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class merchantStockMaster extends Model
{
    protected $table ="merchant_stock_master";
    public function merchantStockVeg(){
       return $this->hasOne('App\merchantStockMaster', 'id');
    }
}
