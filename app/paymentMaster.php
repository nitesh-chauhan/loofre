<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class paymentMaster extends Model
{
    protected $table = 'payment_master';

    protected $fillable = ['status', 'order_status', 'massage'];

	public function payment() {
		return $this->hasOne('App\paymentMaster', 'id');
	}

	public function customer() {
		return $this->hasOne('App\User', 'id', 'userId');
	}
}
