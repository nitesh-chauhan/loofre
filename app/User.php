<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobileNumber', 'password', 'location', 'gender', 'address', 'latitude', 'longitude', 'merchantCode', 'asm_code', 'otp', 'opening_hour', 'closing_hour', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'otp'
    ];

    public function orders()
    {
        return $this->hasMany('App\paymentMaster', 'userId');
    }

    public function categories()
    {
        return $this->belongsToMany('App\categoryMaster', 'category_user', 'user_id', 'category_id');
    }

    public function merchants()
    {
        return $this->belongsToMany('App\User', 'customer_merchant', 'customer_id', 'merchant_id')->with('categories');
    }
}
