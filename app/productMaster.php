<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class productMaster extends Model
{
    protected $table = "product_master";

    protected $fillable = ['price', 'offerPrice', 'stockQuantity', 'unitType'];

    public function productveg() {
       return $this->hasOne('App\productMaster', 'id');
    }

}
