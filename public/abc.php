<!doctype html>
<html lang="en"> 
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script> 
<body>
 
<p>Date: <input type="text" id="datepicker"></p>
 
 
</body>
</html>



<select name="time" class="sel" title="Time">
<option value="" selected="selected">Select Time</option>
<option value="00:00">12:00 AM</option>
<option value="00:30">12:30 AM</option>
<option value="01:00">01:00 AM</option>
<option value="01:30">01:30 AM</option>
<option value="02:00">02:00 AM</option>
<option value="02:30">02:30 AM</option>
<option value="03:00">03:00 AM</option>
<option value="03:30">03:30 AM</option>
<option value="04:00">04:00 AM</option>
<option value="04:30">04:30 AM</option>
<option value="05:00">05:00 AM</option>
<option value="05:30">05:30 AM</option>
<option value="06:00">06:00 AM</option>
<option value="06:30">06:30 AM</option>
<option value="07:00">07:00 AM</option>
<option value="07:30">07:30 AM</option>
<option value="08:00">08:00 AM</option>
<option value="08:30">08:30 AM</option>
<option value="09:00">09:00 AM</option>
<option value="09:30">09:30 AM</option>
<option value="10:00">10:00 AM</option>
<option value="10:30">10:30 AM</option>
<option value="11:00">11:00 AM</option>
<option value="11:30">11:30 AM</option>
<option value="12:00">12:00 PM</option>
<option value="12:30">12:30 PM</option>
<option value="13:00">01:00 PM</option>
<option value="13:30">01:30 PM</option>
<option value="14:00">02:00 PM</option>
<option value="14:30">02:30 PM</option>
<option value="15:00">03:00 PM</option>
<option value="15:30">03:30 PM</option>
<option value="16:00">04:00 PM</option>
<option value="16:30">04:30 PM</option>
<option value="17:00">05:00 PM</option>
<option value="17:30">05:30 PM</option>
<option value="18:00">06:00 PM</option>
<option value="18:30">06:30 PM</option>
<option value="19:00">07:00 PM</option>
<option value="19:30">07:30 PM</option>
<option value="20:00">08:00 PM</option>
<option value="20:30">08:30 PM</option>
<option value="21:00">09:00 PM</option>
<option value="21:30">09:30 PM</option>
<option value="22:00">10:00 PM</option>
<option value="22:30">10:30 PM</option>
<option value="23:00">11:00 PM</option>
<option value="23:30">11:30 PM</option>
</select>