
$(document).ready(function(){
	var city = localStorage.getItem('city');
	var locality = localStorage.getItem('locality');
	if(city){
		$("#cityfixed").html(city);
		$("#localityboxInput").val("");

		getLocalityList(city);
		if(locality){
		$("#cityboxInput").focus();
		$("#localityboxInput").val(locality);
		getLatLong(city,locality);

		
	}
		
	}

    var filterCity = filterCity();
    var filterLocality = filterLocality();
    function filterLocality(){
	$("#localityboxInput").on("focus", function() { 
			$("#localityboxInput").val("");
		
			$("#localityboxInput").on("keyup", function() { 
			$("#localitylist").show();
			var value = $(this).val().toLowerCase();
			$("#localitylist ul *").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});	
		
	});	
		
		$("#localitylist").on( 'click', 'li', function (){
		var lt = $(this).html();
		$("#localityboxInput").val(lt);
		$("#localitylist ul *").hide();
		localStorage.setItem("locality", lt);
		location.reload(true);

		

		});
		
		
		}
    

		
    function filterCity(){
		
		$("#cityfixed").on('click', function(){
		getcityList();
		$("#cityfixed").hide();	
		$("#cityboxInput").show();
		$("#cityboxInput").val("");
		$("#cityboxInput").focus();
		$("#cityboxInput").on("keyup", function() {
			$("#citylist").show();
			var value = $(this).val().toLowerCase();
			$("#citylist ul *").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});	
		
	});
	
	$("#citylist").on( 'click', 'li', function (){
		//alert();
		var ct = $(this).html();
		$("#cityboxInput").val(ct);
		$("#citylist ul *").hide();
		$("#cityfixed").html(ct);
		$("#cityboxInput").hide();
		$("#cityfixed").show();
		localStorage.setItem("city", ct);
		var city = localStorage.getItem('city');
		localStorage.setItem("locality", "Select Locality");
		location.reload(true);
		//getLocalityList();

		

		});
		
		
		
		
}


    


function getcityList(){
	var supplier_id = localStorage.getItem('supplier_id');
	var organizationTypes = localStorage.getItem('organizationTypes');
	//alert(organizationTypes);
	var cityrow='';
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/city",
		dataType:"json",
		//data: { "supplier_id" : supplier_id },
		data: {supplier_id:supplier_id, organizationTypes:organizationTypes},

		success: function(data){
			console.log(data);
		for(i = 0; i < data["loCityobject"].length; i++) {
			if(data["loCityobject"][i].city){
			cityrow += '<li>'+data["loCityobject"][i].city+'</li>';
		    }else{
			cityrow += '<li>'+data["loCityobject"][i].district+'</li>';	
			}
		 //alert(cityrow);
			
		}
$("#cityitem").append(cityrow);

		}
	});
} 
  



function getLocalityList(){
var city = localStorage.getItem('city');
var supplier_id = localStorage.getItem('supplier_id');
var organizationTypes = localStorage.getItem('organizationTypes');
var localityrow='';
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/locality",
		dataType:"json",
		data: {supplier_id:supplier_id, organizationTypes:organizationTypes,city:city,},
		success: function(data){
			//console.log(data);
			for(i = 0; i < data["localityobject"].length; i++) {
				localityrow += '<li>'+data["localityobject"][i].locality+'</li>';

			 //alert(localityrow);
				
			}
			$("#localityitem").append(localityrow);

		}
	});
} 

$("#autoLocationbtn").on('click', function(){//alert("auto1");
	setAutoLocation();
	});
	
function setAutoLocation(){
localStorage.removeItem("city");
localStorage.removeItem("locality");
initMap();
getLocalityList();
location.reload();
}




function getLatLong(city,locality){
if((locality!='')&&(locality!='Select Locality')){	
var address = ""+locality+","+city+"";
var geo = new google.maps.Geocoder;
geo.geocode({'address':address},function(results, status){
    if (status == google.maps.GeocoderStatus.OK) {              
        var myLatLng = results[0].geometry.location;
        var Lat = results[0].geometry.location.lat();
        var Lng = results[0].geometry.location.lng();
		localStorage.setItem("Latitude",Lat);
		localStorage.setItem("Longitude",Lng);
    } else {
        alert("Geocode was not successful for the following reason: " + status);
    }
});	
	
}
}


  
});

