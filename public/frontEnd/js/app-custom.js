	
	
$(document).ready(function(){
	
// ----------------Registeration start------------------------------------		
	$("#code").on("keyup", function() {
	   var SubCode = $(this).val(); 
		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/get-code",
			dataType:"json",
			//data: { "couponCode" : $("#code").val()},
			data: $("#reused_form").serialize(),//only input
			success: function(data){
				//console.log(data );
				if(data.status=='error'){
					$(".loader").show().delay(500).fadeOut();
					$("#msg").html('<span style="color:red">'+data.msg+'</span>');
				}else{
					$(".loader").show().delay(500).fadeOut();
					$("#couponCode").hide().delay(1000);
					$("#code").parent().parent().find(".r-field").css({"display": "block"});
					$("#msg").html('<span style="color:green">'+data.msg+'</span>');
				}
			}
		});
		return false;
	});
	

	$("#sub_btn").on("click", function() {
		$(".loader").show().delay(1000).fadeOut();
		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/sign-up",
			dataType:"json",
			data: $("#reused_form").serialize(),//only input
			success: function(data){
				if(data.status=='error'){
					$("#msg").html('<span style="color:red">'+data.msg+'</span>');
				}else{
					$("#couponCode").hide(500);
					$("#code").parent().parent().find(".r-field").css({"display": "block"});
					$("#msg").html('<span style="color:green">'+data.msg+'</span>');
					var code = $("#code").val();
					var name = $("#name").val();
					var email = $("#email").val();
					var mobile = $("#mobileNumber").val();
					localStorage.setItem("userId", data.userId);
					localStorage.setItem("code", code);
					localStorage.setItem("name", name);
					localStorage.setItem("email", email);
					localStorage.setItem("mobile", mobile);
					$("#regLog").hide().delay(5000);
					setTimeout(function(){ location.reload() }, 500);
				}
			}
		   
		});
		return false;
	});
// ----------------Registeration End------------------------------------
	
// ----------------Allready Have Account - 	Login start------------------------------------
	
	$("#alradyAccTxt").on("click", function() {
		$("#newReg").hide();
		$("#alradyAcc").show();

	});
	$("#backbtn").on("click", function() {
		$("#alradyAcc").hide();
		$("#newReg").show();
	 
	});
	
	$("#Msub_btn").on("click", function() {
		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/sign-in",
			dataType:"json",
			data: $("#Mregisterd_form").serialize(),//only input
			success: function(data){
				if(data.status=='error'){
					$("#msgM").html('<span style="color:red">'+data.msg+'</span>');
				}else{
					$("#msgM").html('<span style="color:green">'+data.msg+'</span>');
					$(".uname").html(data.name);
					localStorage.setItem("name", data.name);
					localStorage.setItem("code", data.couponCode);
					localStorage.setItem("userId", data.userId);
					localStorage.setItem("email", data.email);
					localStorage.setItem("mobile", data.mobileNumber);
					//setTimeout(function(){ location.reload() }, 500);
					getSuplierLogo(data.couponCode);
					var organizationTypes = localStorage.getItem('organizationTypes');
					if((organizationTypes=="Buffet") || (organizationTypes=="Premium-Dining") || (organizationTypes=="Wellness")){
						window.location = "http://www.loofre.co.in/api/fine-dining";
					}
					else{
					setTimeout(function(){ location.reload() }, 500);
					}
					$(".loader").show().delay(600).fadeOut();



				}
			}
		   
		});
		return false;
	});
	
// ----------------Allready Have Account - Login End------------------------------------
// ---------------- Account Details Start - ------------------------------------
	$("#myAccount").on("click", function() {
			$.ajax({
				type: "post",
				url: "http://www.loofre.co.in/api/profile-view",
				dataType:"json",
				data: { "UserId" : localStorage.getItem('userId')},
				success: function(data){
					var account_name = data["profiledataArray"][0].name;
					var account_mobileNumber = data["profiledataArray"][0].mobileNumber;
					var account_email = data["profiledataArray"][0].email;
					var account_couponCode = data["profiledataArray"][0].couponCode;
					var account_couponValidDate = data["profiledataArray"][0].couponValidDate;
					$(".uname").html(account_name);
					$(".contact").html(account_mobileNumber);
					$(".user_email").html(account_email);
					$(".user_Sub_code").html(account_couponCode);
					$(".expiry_date").html(account_couponValidDate);
				}
			});
		}); 

// ----------------Account Details Start End------------------------------------

	
	
});


$(document).ready(function(){

	// ----------------Redirection Tab Start----------------
	var name = localStorage.getItem('name');
	var code = localStorage.getItem('code');
	var supplier_id = localStorage.getItem('supplier_id');
	if(name){
    	$(".uname").html(name);
		$(document).on('click','#Home,#Buffet,#QSR,#Dining,#Wellness',function(){
			var organizationTypes = $(this).attr("id");
			localStorage.setItem("organizationTypes", organizationTypes);
			if(organizationTypes=="Home"){
				window.location = "http://www.loofre.co.in/";
				}else if(organizationTypes=="Wellness"){
				window.location = "http://www.loofre.co.in/api/wellness";
				}else if(organizationTypes=="Buffet"){
				window.location = "http://www.loofre.co.in/api/buffet";
				}else{
				window.location = "http://www.loofre.co.in/api/fine-dining";
				}
		});
		getSuplierLogo(code);

	}
     else{	
	  $(document).on('click','#accn,#QSR,#Buffet,#Dining,#Wellness,#camp1,#camp2,#camp3',function(){

		  
		  
		  
		var organizationTypes = $(this).attr("id");
		localStorage.setItem("organizationTypes", organizationTypes);
		$(this).find("a").attr({ target: '',href: '#'});
		$('#regLog').modal('show'); 
		
		});	 

		$(document).on('click','#Home',function(){
			location.reload(true);
		});
		
		
      }
// ----------------Redirection Tab End--------------------
// ----------------Logout Section Start--------------------
	 
	$("#logout").on('click',function(){
	  localStorage.clear();
	  //location.reload(true);
	  window.location = "http://www.loofre.co.in/";

	  });
 // ----------------Show Vendor CouponCode Start--------------------
 
 	$("#eee").on('click',function(){

			alert("unlock-offer");
	  });
	  


  
});

// ----------------Menu section start------------------------------------

$(document).ready(function(){
	get_top_menu();
	get_bottom_menu();

	function get_bottom_menu(){
		var menuB ="";
		var BaseURL = "http://www.loofre.co.in/"; 
			$.ajax({
			type: "Get",
			url: "http://www.loofre.co.in/api/bottom-menu",
			dataType:"json",
			//data: $("#Mregisterd_form").serialize(),//only input
			success: function(data){
				for (i = 0; i < data["BottomMenu"].length; i++) {
					var menuTitle = data["BottomMenu"][i].menuTitle;
					var menuTitle = menuTitle.replace(" ", "-");
					var menuImage = data["BottomMenu"][i].menuImage;
					var linkName = data["BottomMenu"][i].linkName;
					menuB += '<div class="m-box active" id="'+menuTitle+'"><a class="" href="#"><img class="m_item" src="'+BaseURL+''+menuImage+'"></a><span class="cat_name"><a href="#">'+menuTitle+'</a></span></div>';  
				}
				$("#menuBottom").append(menuB);
			}
			});
		return false;
	}
	
	function get_top_menu(){
		var menuT ="";
		var BaseURL = "http://www.loofre.co.in/"; 
		$.ajax({
			type: "Get",
			url: "http://www.loofre.co.in/api/top-menu",
			dataType:"json",
			//data: $("#Mregisterd_form").serialize(),//only input
			success: function(data){
				for (i = 0; i < data["TopMenu"].length; i++) {
				var menuTitle = data["TopMenu"][i].menuTitle;
				var menuImage = data["TopMenu"][i].menuImage;
				var linkName = data["TopMenu"][i].linkName;
				menuT += '<div class="m-box" id="'+menuTitle+'"><a href="'+linkName+'"><img class="m_item" src="'+BaseURL+''+menuImage+'"></a><span class="cat_name"><a href="'+linkName+'">'+menuTitle+'</a></span></div>';  
				}
				$("#topMenuScroll").append(menuT);
			}
		});
	return false;
	}
 
  
});
// ----------------Menu section End-----------------
// ----------------Get Suplier info start------------
function getSuplierLogo(code){
	
	var BaseURL = "http://www.loofre.co.in/"; 
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/supplier-logo",
		dataType:"json",
		data: { "couponCode" : code },
		success: function(data){
			var sup_logo = BaseURL+data.logo;
			$(".logo").find("img").attr({ src:sup_logo});
			$("#sublier_name").html(data.supplierName);
			localStorage.setItem("supplier_id", data.supplier_id);
			localStorage.setItem("supplierName", data.supplierName);
			localStorage.setItem("aliasName", data.aliasName);

		}
	});
	return false;	
}

// ----------------Get Suplier info End---------------



function getVendorOutletList(organizationTypes,supplier_id,city,locality){
		//window.location = "http://phplaravel-179910-524161.cloudwaysapps.com/api/fine-dining";
		var BaseURL = "http://www.loofre.co.in/";
        var vendorDiv = "";
        var i = "";
		$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-vender",
		dataType:"json",
		data: {supplier_id:supplier_id, organizationTypes:organizationTypes,city:city,locality:locality},
		success: function(data){
			if((data["restaurantData"].length)>0){
				for (i = 0; i < data["restaurantData"].length; i++) {
					var bookigStatus = ((data["restaurantData"][i].bookingStatus).replace(",", " "));	
					//alert(bookigStatus);
					var vendorIMG = BaseURL+data["restaurantData"][i].path;
					vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6 listing-box">';
						vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
							vendorDiv += '<div class="list_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
				vendorDiv += '<div class="discountOffered">'+parseInt(data["restaurantData"][i].discount)+'% off</div>';
								vendorDiv += '<img itemprop="image" src="'+vendorIMG+'" alt="Party Booking">';
							vendorDiv += '</div>';
							vendorDiv += '<div class="list-info">';
								vendorDiv += '<div class="info">';
									vendorDiv += '<h3  itemprop="name" title="'+data["restaurantData"][i].outletName+'">'+data["restaurantData"][i].outletName+'</h3>';
									vendorDiv += '<h4 itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
									vendorDiv += '<span itemprop="addressLocality">'+data["restaurantData"][i].locality+'</span>, <span itemprop="addressRegion">'+data["restaurantData"][i].city+'</span>';
									vendorDiv += '</h4>';
									vendorDiv += '<p itemprop="servesCuisine" class="cuisine" >'+data["restaurantData"][i].cuisine+'</p>';
								vendorDiv += '</div>';
								vendorDiv += '<div class="col-lg-12 list-tabs '+bookigStatus+' '+organizationTypes+'">';
									vendorDiv += '<div  class="col-xs-6 btn btn-offer btn-md unlock-offer" data-toggle="modal" data-target="#unlockCode" onclick="unlockOfferOutlet('+data["restaurantData"][i].venderOutletsId+','+parseInt(data["restaurantData"][i].discount)+')">Unlock Offer</div>';
								vendorDiv += '<div id="bookBtn" class="col-xs-6 btn btn-book btn-md" data-toggle="modal" data-target="#dining_booking" onclick="bookDiningOutlet('+data["restaurantData"][i].venderOutletsId+','+parseInt(data["restaurantData"][i].discount)+')">Book Now</div>';
								vendorDiv += '</div>';
							vendorDiv += '</div>';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
			
				}
				$("#vendor_list").append(vendorDiv);
			}else{
				vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6">';
					vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
						vendorDiv += '<div class="not_found_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
							vendorDiv += '<img itemprop="image" src="http://www.loofre.co.in/images/not_found.jpg" alt="Not Found">';
						vendorDiv += '</div>';
						vendorDiv += '<div class="list-info">';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
					vendorDiv += '<div id="localitylist1"><h4>Offers available @ other location</h4></div>';
				vendorDiv += '</div>';
				
				$("#vendor_list").append(vendorDiv);
				$("#localitylist").show();
				$("#localitylist").css({"margin-top":"340px"});
				$("#vendor_list").css({"background":"none"});

			}	
		}

		
	});
	return false;	


	}

function getVendorOutletBuffetList(organizationTypes,supplier_id,city,locality){
		var BaseURL = "http://www.loofre.co.in/";
        var vendorDiv = "";
        var i = "";
		$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-vender",
		dataType:"json",
		data: {supplier_id:supplier_id, organizationTypes:organizationTypes,city:city,locality:locality},
		success: function(data){
			if((data["restaurantData"].length)>0){
				for (i = 0; i < data["restaurantData"].length; i++) {
					var bookigStatus = ((data["restaurantData"][i].bookingStatus).replace(",", " "));	
					//alert(bookigStatus);
					var vendorIMG = BaseURL+data["restaurantData"][i].path;
					vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6 listing-box">';
						vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
							vendorDiv += '<div class="list_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
				vendorDiv += '<div class="discountOffered">'+parseInt(data["restaurantData"][i].discount)+'% off</div>';
								vendorDiv += '<img itemprop="image" src="'+vendorIMG+'" alt="Party Booking">';
							vendorDiv += '</div>';
							vendorDiv += '<div class="list-info">';
								vendorDiv += '<div class="info">';
									vendorDiv += '<h3  itemprop="name" title="'+data["restaurantData"][i].outletName+'">'+data["restaurantData"][i].outletName+'</h3>';
									vendorDiv += '<h4 itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
									vendorDiv += '<span itemprop="addressLocality">'+data["restaurantData"][i].locality+'</span>, <span itemprop="addressRegion">'+data["restaurantData"][i].city+'</span>';
									vendorDiv += '</h4>';
									vendorDiv += '<p itemprop="servesCuisine" class="cuisine" >'+data["restaurantData"][i].cuisine+'</p>';
								vendorDiv += '</div>';
								vendorDiv += '<div class="col-lg-12 list-tabs '+bookigStatus+' '+organizationTypes+'">';
									vendorDiv += '<div  class="col-xs-6 btn btn-offer btn-md unlock-offer" data-toggle="modal" data-target="#unlockCode" onclick="unlockOfferOutlet('+data["restaurantData"][i].venderOutletsId+','+parseInt(data["restaurantData"][i].discount)+')">Unlock Offer</div>';
								vendorDiv += '<div id="bookBtn" class="col-xs-6 btn btn-book btn-md" data-toggle="modal" data-target="#buffet_booking" onclick="bookBuffetOutlet('+data["restaurantData"][i].venderOutletsId+','+parseInt(data["restaurantData"][i].discount)+')">Book Now</div>';
								vendorDiv += '</div>';
							vendorDiv += '</div>';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
			
				}
				$("#vendor_list").append(vendorDiv);
			}else{
				vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6">';
					vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
						vendorDiv += '<div class="not_found_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
							vendorDiv += '<img itemprop="image" src="http://www.loofre.co.in/images/not_found.jpg" alt="Not Found">';
						vendorDiv += '</div>';
						vendorDiv += '<div class="list-info">';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
					vendorDiv += '<div id="localitylist1"><h4>Offers available @ other location</h4></div>';
				vendorDiv += '</div>';
				
				$("#vendor_list").append(vendorDiv);
				$("#localitylist").show();
				$("#localitylist").css({"margin-top":"340px"});
				$("#vendor_list").css({"background":"none"});

			}	
		}

		
	});
	return false;	


	}
	
	function getWellnessOutletList(supplier_id,city,locality){
		var BaseURL = "http://www.loofre.co.in/";
        var vendorDiv = "";
        var i = "";
		$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-spa",
		dataType:"json",
		data: {supplier_id:supplier_id,city:city,locality:locality},
		success: function(data){
			if((data["wellnessData"].length)>0){
				for (i = 0; i < data["wellnessData"].length; i++) {
					var vendorIMG = BaseURL+data["wellnessData"][i].path;
					vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6 listing-box">';
						vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
							vendorDiv += '<div class="list_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
								vendorDiv += '<div class="discountOffered">'+parseInt(data["wellnessData"][i].discount)+'% off</div>';
								vendorDiv += '<img itemprop="image" src="'+vendorIMG+'" alt="Party Booking">';
							vendorDiv += '</div>';
							vendorDiv += '<div class="list-info">';
								vendorDiv += '<div class="info">';
									vendorDiv += '<h3  itemprop="name" title="'+data["wellnessData"][i].name+'">'+data["wellnessData"][i].name+'</h3>';
									vendorDiv += '<h4 itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">';
									//vendorDiv += '<span itemprop="addressLocality">'+data["wellnessData"][i].town+'</span>, <span itemprop="addressRegion">'+data["wellnessData"][i].district+'</span>';
									vendorDiv += '<span itemprop="addressLocality">'+data["wellnessData"][i].address+'</span>';
									vendorDiv += '</h4>';
									vendorDiv += '<p itemprop="servesCuisine" class="cuisine" >'+data["wellnessData"][i].type+'</p>';
								vendorDiv += '</div>';
								vendorDiv += '<div class="col-lg-12 list-tabs wellness">';
									vendorDiv += '<div  class="col-xs-6 btn btn-offer btn-md unlock-offer" data-toggle="modal" data-target="#unlockCode" onclick="unlockOfferSpa('+data["wellnessData"][i].id+','+parseInt(data["wellnessData"][i].discount)+')">Unlock Offer</div>';
									vendorDiv += '<div id="bookBtn" class="col-xs-6 btn btn-book btn-md">Book Now</div>';
								vendorDiv += '</div>';
							vendorDiv += '</div>';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
		
			}
			$("#vendor_list").append(vendorDiv);
				
				
			}else{
				vendorDiv += '<div class="col-lg-4 col-md-6 col-sm-6">';
					vendorDiv += '<div class="list-data" itemscope itemtype="http://schema.org/Restaurant">';
						vendorDiv += '<div class="not_found_img" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">';
							vendorDiv += '<img itemprop="image" src="http://www.loofre.co.in/images/not_found.jpg" alt="Not Found">';
						vendorDiv += '</div>';
						vendorDiv += '<div class="list-info">';
						vendorDiv += '</div>';
					vendorDiv += '</div>';
					vendorDiv += '<div id="localitylist1"><h4>Offers available @ other location</h4></div>';
				vendorDiv += '</div>';
				
				$("#vendor_list").append(vendorDiv);
				$("#localitylist").show();
				$("#localitylist").css({"margin-top":"340px"});
				$("#vendor_list").css({"background":"none"});
				
				}



		}
		
	});
	return false;	


	}
	
	
$(document).ready(function refreshdata(){
	var name = localStorage.getItem('name');
	var code = localStorage.getItem('code');
	var supplier_id = localStorage.getItem('supplier_id');
	var organizationTypes = localStorage.getItem('organizationTypes');
	var city = localStorage.getItem('city');
	var locality = localStorage.getItem('locality');

	//alert(organizationTypes);
	setTimeout(function(){ activeTabs(organizationTypes) },400);
	if(name!='' && organizationTypes=='Buffet'){
			getVendorOutletBuffetList(organizationTypes,supplier_id,city,locality);

		}
	else if(name!='' && organizationTypes=='Dining'){
		    //alert("Premium-Dining");
		    //organizationTypes = "Dining";
			getVendorOutletList(organizationTypes,supplier_id,city,locality);	

		}	
	else if(name!='' && organizationTypes=='Wellness'){
			getWellnessOutletList(supplier_id,city,locality);	

		}
	else if(name!='' && organizationTypes=='QSR'){
			getVendorOutletList(organizationTypes,supplier_id,city,locality);	
		}


		
});


function unlockOfferOutlet(venderOutletsId,discount){
	var userId = localStorage.getItem('userId');
	var supplier_id = localStorage.getItem('supplier_id');
	var Suplier_aliasName = localStorage.getItem('aliasName');
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-coupon",
		dataType:"json",
		data: {userId:userId, venderOutletsId:venderOutletsId, supplier_id:supplier_id, aliasName:Suplier_aliasName},
		success: function(data){
			$("#vendor_code").html(data.vendorCouponCode);
			//$(".discountSec").html(data.discount);
			$(".discountSec").html(''+discount+'% off on Total Bill');
			$("#vender_name").html(data.outletName);
			//$("#vendor_logo img").attr({ src:'http://www.loofre.co.in/'+data.path+''});
		}
	});
	
	}
	
function unlockOfferSpa(venderOutletsId,discount){
	var userId = localStorage.getItem('userId');
	var supplier_id = localStorage.getItem('supplier_id');
	var Suplier_aliasName = localStorage.getItem('aliasName');
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-spa-coupon",
		dataType:"json",
		data: {userId:userId, spaVenderId:venderOutletsId, supplier_id:supplier_id, aliasName:Suplier_aliasName},
		success: function(data){
			console.log(data);
			$("#vendor_code").html(data.vendorCouponCode);
			//$(".discountSec").html(data.discount);
			$(".discountSec").html(''+discount+'% off on Total Bill');
			$("#vender_name").html(data.outletName);
			//$("#vendor_logo img").attr({ src:'http://www.loofre.co.in/'+data.path+''});
		}
	});
	
	}
	
function activeTabs(organizationTypes){
	if(organizationTypes=="Home"){ 
		 // alert(organizationTypes);
		$("#"+organizationTypes+".active").css({"box-shadow": "#0097d9 1px -17px 41px 2px inset"});
		 $("#"+organizationTypes+".active").find(".cat_name a").css({"color": "#fff"});
		 }
	else if(organizationTypes=="QSR"){ 
		 // alert(organizationTypes);
		$("#"+organizationTypes+".active").css({"box-shadow": "#f58634 1px -17px 41px 2px inset"});
		 $("#"+organizationTypes+".active").find(".cat_name a").css({"color": "#fff"});
		 }
	else if(organizationTypes=="Dining"){ 
		 // alert(organizationTypes);
		$("#"+organizationTypes+".active").css({"box-shadow": "rgb(237, 50, 55) 1px -17px 41px 2px inset"});
		 $("#"+organizationTypes+".active").find(".cat_name a").css({"color": "#fff"});
		 }
	else if(organizationTypes=="Buffet"){ 
		 // alert(organizationTypes);
		$("#"+organizationTypes+".active").css({"box-shadow": "#0097d9 1px -17px 41px 2px inset"});
		 $("#"+organizationTypes+".active").find(".cat_name a").css({"color": "#fff"});
		 }
	else if(organizationTypes=="Wellness"){ 
		 // alert(organizationTypes);
		$("#"+organizationTypes+".active").css({"box-shadow": "#f58634 1px -17px 41px 2px inset"});
		 $("#"+organizationTypes+".active").find(".cat_name a").css({"color": "#fff"});
		 }


	
	}			


// ----------- Get Lat Long From Url Start------------

$(document).ready(function(){ 
	var sPageURL = window.location.search.substring(1);
	var URL = window.location;
   		//alert(URL);
   		//alert(sPageURL);

    if(sPageURL){
		var lat = getURLParam("lat");
		var long = getURLParam("long");
		localStorage.setItem("Latitude", lat);
		localStorage.setItem("Longitude", long);
	}else{
		
		//localStorage.setItem("Latitude", "28.6315");
		//localStorage.setItem("Longitude", "77.2167");
		var latitude = localStorage.getItem('Latitude');
		var longitude = localStorage.getItem('Longitude');
		}
		
});


function getURLParam(param){

	var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
    {
       var sParameterName = sURLVariables[i].split('=');
      	if (sParameterName[0] == param){
			//alert(sParameterName[1]);
			return sParameterName[1];
			} 

   }
	
	}
// ----------- Get Lat Long From Url End------------


