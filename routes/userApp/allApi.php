<?php
//@manjesh app App All Api
//***********Loofre Veg  start**************************************************************
Route::get('api/outlet-categories', ['as' => 'api/outlet-categories', 'uses' => 'ApiController@outletCategories']);
Route::get('api/product-categories', ['as' => 'api/product-categories', 'uses' => 'ApiController@productCategories']);
Route::get('api/unit-categories', ['as' => 'api/unit-categories', 'uses' => 'ApiController@unitCategories']);

Route::post('api/menu-list', ['as' => 'api/menu-list', 'uses' => 'ApiController@menuList']);
Route::post('api/product-list-info', ['as' => 'product-list-info', 'uses' => 'ApiController@productListInfo']);
Route::post('api/sign-up', ['as' => 'api/sign-up', 'uses' =>'ApiController@signUp']);
Route::post('api/sinup-customer', ['as' => 'sinup-customer', 'uses' => 'ApiController@sinupCustomer']);
Route::post('api/login', ['as' => 'login', 'uses' => 'ApiController@login']);
Route::post('api/merchant-info', ['as' => 'merchant-info', 'uses' => 'ApiController@merchantInfo']);
Route::post('api/profile-update', ['as' => 'profile-update', 'uses' => 'ApiController@profileUpdate']);
Route::get('api/merchant-unit', ['as' => 'merchant-unit', 'uses' => 'ApiController@merchantUnit']);
Route::get('api/merchant-communations', ['as' =>'merchant-communations', 'uses' => 'ApiController@merchantCommunations']);
Route::post('api/merchant-stock-save', ['as' => 'merchant-stock-save', 'uses' => 'ApiController@merchantStockSave']);
Route::post('api/product-list-customer', ['as' => 'product-list-customer', 'uses' => 'ApiController@productListCustomer']);
Route::post('api/quantaty-update-customer', ['as' => 'quantaty-update-customer', 'uses' => 'ApiController@quantatyUpdateCustomer']);
Route::post('api/merchant-product-list', ['as' => 'merchant-product-list', 'uses' => 'ApiController@merchantProductList']);
Route::post('api/add-cart', ['as' => 'add-cart', 'uses' => 'ApiController@addCart']);
Route::post('api/cart-list', ['as' => 'cart-list', 'uses' => 'ApiController@cartList']);
Route::post('api/cart-delete', ['as' => 'cart-delete', 'uses' => 'ApiController@cartDelete']);
Route::post('api/place-order', ['as' => 'place-order', 'uses' => 'ApiController@placeOrder']);
Route::post('api/place-order-cash', ['as' => 'place-order-cash', 'uses' => 'ApiController@placeOrderCash']);
Route::post('api/details-order', ['as' => 'details-order', 'uses' => 'ApiController@detailsOrder']);
Route::post('api/place-order-cancal', ['as' => 'place-order-cancal', 'uses' => 'ApiController@placeOrderCancal']);


Route::post('api/customer-order-list', ['as' => 'customer-order-list', 'uses' => 'ApiController@customerOrderList']);
Route::post('api/merchant-order-list', ['as' => 'merchant-order-list', 'uses' => 'ApiController@merchantOrderList']);
Route::post('api/merchant-customer-communations', ['as' =>'merchant-customer-communations', 'uses' => 'ApiController@merchantCustomerCommunations']);
Route::post('api/merchant-product-status', ['as' => 'merchant-product-status', 'uses' => 'ApiController@merchantProductStatus']);

Route::post('api/search', ['as' => 'search', 'uses' => 'ApiController@search']);
Route::post('api/user-cancel-order', ['as' => 'user-cancel-order', 'uses' => 'ApiController@userCancelOrder']);
Route::post('api/order-timing', ['as' => 'order-timing', 'uses' => 'ApiController@orderTiming']);
Route::post('api/order-message', ['as' => 'order-message', 'uses' => 'ApiController@orderMessage']);

Route::post('api/send-password-sms', ['as' => 'send-password-sms', 'uses' => 'ApiController@sendPasswordSms']);
Route::post('api/verify-otp', ['as' => 'verify-otp', 'uses' => 'ApiController@verifyOtp']);
Route::post('api/reset-password', ['as' => 'reset-password', 'uses' => 'ApiController@resetPassword']);

Route::post('api/customer-change-vendor', ['as' => 'customer-change-vendor', 'uses' => 'ApiController@changeVendor']);
Route::post('api/customer-create-vendor', ['as' => 'customer-create-vendor', 'uses' => 'ApiController@customerCreateVendor']);
Route::post('api/customer-vendors', ['as' => 'customer-vendors', 'uses' => 'ApiController@customerVendors']);

//***********Loofre Veg  end**************************************************************