<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Events\FormSubmitted;
Route::get('/counter', function(){
return view('counter');
});
Route::get('/sender', function(){
return view('sender');
});
Route::post('/sender', function(Request $request){
	$text = request()->text;

event(new FormSubmitted($text));
});

include("webPage/webPage.php");
Route::get('/crm', function () {  return view('login'); });
Route::post('/emp-login', ['as' => 'emp-login', 'uses' => 'AuthController@empLogin']);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
//******************Offer Api hear by manjesh end**************************************************************/
//******************app api hear by manjesh start**************************************************************/
include("userApp/allApi.php");
//******************app api hear by manjesh end**************************************************************/
//test routes please dont change! by manjesh
Route::get('/test', function(){return view('welcome');});
/**************After Login Superadmin**************/
Auth::routes();
/************** Login Superadmin End**************/
Route::get('/home', 'HomeController@index')->name('home');

// Route::get('change/password/{mobile}', 'ApiController@resetPasswordByWeb')->name('password.change');
