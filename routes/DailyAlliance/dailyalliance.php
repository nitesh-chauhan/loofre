<?php 
Route::get('pending-alliance-list', ['as' => 'pending-alliance-list', 'uses' =>'DailyAlliance@pendingAllianceList']);
Route::get('add-daily-alliance', ['as' => 'add-daily-alliance', 'uses' =>'DailyAlliance@addDailyAlliance']);
Route::post('daily-alliance-save', ['as' => 'daily-alliance-save', 'uses' =>'DailyAlliance@dailyAllianceSave']);
 
?>