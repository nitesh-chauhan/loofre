@extends('layouts.front')
@section('content')
<body id="bds" class="home page-template page-template-frontpage page-template-frontpage-php page page-id-3894 mmm mega_main_menu-2-1-3 wpb-js-composer js-comp-ver-5.0.1 vc_responsive" cz-shortcut-listen="true">
<div id="iframe_content">

    <div id="all_content" class=" boxed_width    fixed_sidebar  ">
		@include('FrontWebPage.includes.header_inner')
        <div class="mobile_menu">
            <div class="scroll" id="topMenuScroll">   
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div id="menu_box">

        <!-- begin "mega_main_menu" -->
        <div id="mega_main_menu" class="main-menu primary_style-flat icons-left first-lvl-align-left first-lvl-separator-smooth direction-horizontal fullwidth-disable pushing_content-disable mobile_minimized-enable dropdowns_trigger-hover dropdowns_animation-none no-logo no-search no-woo_cart no-buddypress responsive-enable coercive_styles-disable indefinite_location_mode-disable language_direction-ltr version-2-1-3 mega_main mega_main_menu" style="z-index: 990;">
            <div class="menu_holder">
                <div class="mmm_fullwidth_container"></div>
                <!-- class="fullwidth_container" -->
                <div class="menu_inner">
                    <span class="nav_logo">
				<a class="mobile_toggle">
					<span class="mobile_button">
						Menu &nbsp;
						<span class="symbol_menu">≡</span>
                    <span class="symbol_cross">╳</span>
                    </span>
                    <!-- class="mobile_button" -->
                    </a>
                    </span>
                    <!-- /class="nav_logo" -->
                    <ul id="mega_main_menu_ul" class="mega_main_menu_ul">
                        <li id="menu-item-16291" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16291 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a href="https://www.loofre.com/" class="item_link  with_icon" tabindex="1">
                                <i class="im-icon-home-6"></i>
                                <span class="link_content">
			<span class="link_text">
				Home
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-15738" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-15738 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a href="#" class="item_link  disable_icon" tabindex="2">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				DJ
			</span>
                                </span>
                            </a>
                            <ul class="mega_dropdown">
                                <li id="menu-item-14918" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-14918 default_dropdown default_style drop_to_right submenu_default_width columns1">
                                    <a href="https://www.loofre.com/irock/dj" class="item_link  disable_icon" tabindex="3">
                                        <i class=""></i>
                                        <span class="link_content">
				<span class="link_text">
					Female dj
				</span>
                                        </span>
                                    </a>
                                </li>
                                <li id="menu-item-10231" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10231 default_dropdown default_style drop_to_right submenu_default_width columns1">
                                    <a target="_blank" href="https://www.loofre.com/irock/india_dj" class="item_link  disable_icon" tabindex="4">
                                        <i class=""></i>
                                        <span class="link_content">
				<span class="link_text">
					Male dj
				</span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <!-- /.mega_dropdown -->
                        </li>
                        <li id="menu-item-10232" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10232 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/band" class="item_link  disable_icon" tabindex="5">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Band
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-10233" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-10233 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/chef" class="item_link  disable_icon" tabindex="6">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				chef
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-11010" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11010 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/foodie" class="item_link  disable_icon" tabindex="7">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				bloggers
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-11012" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11012 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/percussionist" class="item_link  disable_icon" tabindex="8">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Percussionist
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-11323" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-11323 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/comedian" class="item_link  disable_icon" tabindex="9">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Comedian
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-11551" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11551 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/loofre-diaries" class="item_link  disable_icon" tabindex="10">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Loofre Diaries
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-12461" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12461 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/fashion" class="item_link  disable_icon" tabindex="11">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Fashion
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-12918" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12918 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/singer" class="item_link  disable_icon" tabindex="12">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Singers
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-13949" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13949 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a target="_blank" href="https://www.loofre.com/irock/tattoo_artist" class="item_link  disable_icon" tabindex="13">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Tattoo Artist
			</span>
                                </span>
                            </a>
                        </li>
                        <li id="menu-item-15740" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-15740 default_dropdown default_style drop_to_right submenu_default_width columns1">
                            <a href="https://www.loofre.com/irock/author" class="item_link  disable_icon" tabindex="14">
                                <i class=""></i>
                                <span class="link_content">
			<span class="link_text">
				Authors
			</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /class="menu_inner" -->
            </div>
            <!-- /class="menu_holder" -->
        </div>
        <!-- /id="mega_main_menu" -->
    </div>
    <div class="clear"></div>

    <div class="inner_woo">
        <div id="main_content" class="home_page">

            <div class="camp-box">

                <div class="camp-list">
                    <div class="tagline">
                        <p>Special Offers & Free Reservation </p>

                    </div>
                    <div class="item-box" id="Dining">
                        <div id="camp1" class="item">
							<span id="dining_icon" class="tabicon"></span>
                            <a href="#">  Dining Offers </a>
                        </div>
                    </div>
                    <div class="item-box" id="Buffet">
                        <div id="camp2" class="item">
							<span id="buffet_icon" class="tabicon"></span>
                            <a href="#">  Buffet Offers </a>
                        </div>
                    </div>
                    <div class="item-box" id="Wellness">
                        <div id="camp3" class="item">
							<span id="wellness_icon" class="tabicon"></span>
                            <a href="#">  Wellness Offers </a>
                        </div>
                    </div>
                    <div class="item-box" id="QSR">
                        <div id="camp4" class="item">
							<span id="dining_icon" class="tabicon" ></span>
                            <a href="#">  QSR </a>
                        </div>
                    </div>
                     <div class="tagline">
						<p id="membersonly">#MembersOnly</p>
                    </div>

                </div>
                <div class=""></div>

            </div>


            <div id="moble_view_post">
                <div class="inner grid_posts_container">
                    <div class="grid_left_box" id="grid_left_box">

                    </div>
                </div>
                <div class="clear"></div>

            </div>

            <div class="inner">

            <!-- ff -->
			<div id="sidebar-small" class="small_sidebar_left">
                    <div class="small-widget" id="recentcategorypostssmall-3">
                        <div class="small-heading">
                            <h3><span class="first_word">Fashion</span>Blogger</h3></div>
                        <div class="clear"></div>
                        <ul class="widget_recent_posts_small" id="left_sidebar1">
                        </ul>
                    </div>
                    <div class="small-widget" id="recentcategorypostssmall-2">
                        <div class="small-heading">
                            <h3><span class="first_word">Food </span>Blogger</h3></div>
                        <div class="clear"></div>
                        <ul class="widget_recent_posts_small" id="left_sidebar2">
                        </ul>
                    </div>
                    <div class="small-widget" id="smallsidebar-5">
                        <div class="small-heading">
                            <h3><span class="first_word">Female </span>DJs</h3></div>
                        <div class="clear"></div>
                        <ul class="widget_recent_posts_two" id="left_sidebar3">
                        </ul>
                    </div>
                    <div class="small-widget" id="smallsidebar-7">
                        <div class="small-heading">
                            <h3><span class="first_word">FEATURED </span>COMEDIAN</h3></div>
                        <div class="clear"></div>
                        <ul class="widget_recent_posts_two" id="left_sidebar4">    
                        </ul>
                    </div>
                    <div class="small-widget" id="smallsidebar-8">
                        <div class="small-heading">
                            <h3><span class="first_word">Tattoo </span>Artist</h3></div>
                        <div class="clear"></div>
                        <ul class="widget_recent_posts_two" id="left_sidebar5">  
                        </ul>
                    </div>

                </div>

                <div id="home_content" class=" ">

                    <div id="home_content_inner">

                        <div class="">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_video_widget wpb_content_element vc_clearfix   vc_video-aspect-ratio-169 vc_video-el-width-100 vc_video-align-left">
                                            <div class="wpb_wrapper">
                                                <h2 class="wpb_heading wpb_video_heading">Loofre Chef Web Series</h2>
                                                <div class="wpb_video_wrapper">
												<iframe width="560" height="315" src="https://www.youtube.com/embed/0fSnTeFEBsk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="DesktopView" class="vc_row wpb_row vc_row-fluid">

                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="shortcode_box wpb_content_element ">

                                            <div class="home_posts_title fashion" style="border-color: #0098da;">
                                                <h2><a href="https://www.loofre.com/irock/fashion">FASHION BLOGGER</a></h2>
                                                <div class="car_title_descr"></div>
                                            </div>
                                            <div class="clear"></div>
                                            <div id="home_grid_posts" class="fashion">

                                                <div class="grid_post first_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/fashion-whatshevoice-by-ishi" title="Permalink to WhatSheVoice by Ishi Gahlaut"><img src="{{asset('/images/frontImage/HomePage') }}/IshiGahlaut-001-768x500.jpg" alt="WhatSheVoice by Ishi Gahlaut" title="WhatSheVoice by Ishi Gahlaut" width="815" height="500"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/fashion-whatshevoice-by-ishi" title="Permalink to WhatSheVoice by Ishi Gahlaut">WhatSheVoice by Ishi Gahlaut</a></h2>

                                                    </div>
                                                </div>
                                                <div class="grid_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/fashion-plums-peplum-kompal-matta" title="Permalink to Plums &amp; Peplum by Kompal Matta"><img src="{{asset('/images/frontImage/HomePage') }}/Kompal-3-400x230.jpg" alt="Plums &amp; Peplum by Kompal Matta" title="Plums &amp; Peplum by Kompal Matta" width="400" height="230"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/fashion-plums-peplum-kompal-matta" title="Permalink to Plums &amp; Peplum by Kompal Matta">Plums &amp; Peplum by Kompal Matta</a></h2>

                                                    </div>
                                                </div>
                                                <div class="grid_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/fashion-chic-gazette-vaishali-jain" title="Permalink to The Chic Gazette by Vaishali Jain"><img src="{{asset('/images/frontImage/HomePage') }}/DSC_0093-e1523268571894-400x230.jpg" alt="The Chic Gazette by Vaishali Jain" title="The Chic Gazette by Vaishali Jain" width="400" height="230"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/fashion-chic-gazette-vaishali-jain" title="Permalink to The Chic Gazette by Vaishali Jain">The Chic Gazette by Vaishali Jain</a></h2>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        
                                        <div class="shortcode_box wpb_content_element ">

                                            <div class="home_posts_title chef" style="border-color: #ed3237;">
                                                <h2><a href="https://www.loofre.com/irock/chef">FEATURED CHEFS</a></h2>
                                                <div class="car_title_descr">
                                                    <p>Chefs to be listed on loofre</p>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div id="home_grid_posts" class="chef">

                                                <div class="grid_post first_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/chef-vikas-shirivastva" title="Permalink to Chef Vikas Shirivastva"><img src="{{asset('/images/frontImage/HomePage') }}/MG_311021-e1527674276901-815x500.jpg" alt="Chef Vikas Shirivastva" title="Chef Vikas Shirivastva" width="815" height="500"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/chef-vikas-shirivastva" title="Permalink to Chef Vikas Shirivastva">Chef Vikas Shirivastva</a></h2>

                                                    </div>
                                                </div>
                                                <div class="grid_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/chef-meghshyam-sawal" title="Permalink to Chef Meghshyam Sawal"><img src="{{asset('/images/frontImage/HomePage') }}/chef-3-400x230.jpg" alt="Chef Meghshyam Sawal" title="Chef Meghshyam Sawal" width="400" height="230"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/chef-meghshyam-sawal" title="Permalink to Chef Meghshyam Sawal">Chef Meghshyam Sawal</a></h2>

                                                    </div>
                                                </div>
                                                <div class="grid_post">
                                                    <div class="short_image_box">
                                                        <a class="link_title" href="https://www.loofre.com/irock/chef-tarang-bhargava-hyatt-place-hampi" title="Permalink to Chef Tarang Bhargava- Hyatt Place Hampi"><img src="{{asset('/images/frontImage/HomePage') }}/Tarang-Chef-1-400x230.jpg" alt="Chef Tarang Bhargava- Hyatt Place Hampi" title="Chef Tarang Bhargava- Hyatt Place Hampi" width="400" height="230"> </a>
                                                    </div>
                                                    <div class="grid_shadow_box">
                                                        <h2 class="grid_title"><a class="link_title" href="https://www.loofre.com/irock/chef-tarang-bhargava-hyatt-place-hampi" title="Permalink to Chef Tarang Bhargava- Hyatt Place Hampi">Chef Tarang Bhargava- Hyatt Place Hampi</a></h2>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <div class="contentCardsIRock">
                                                    <h2 class="wpb_heading"><a href="https://www.loofre.com/eatopia">Photostories At Eatopia</a></h2>
                                                    <div class="contentCardListIRock" id="eatopia_post">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>



            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>

    <div class="clear"></div>

    <br>
    <div class="clear"></div>
    <div id="footer_bottom">
    </div>

    <footer>
        <div id="footer-top" style="background:#363636;">
            <div class="footer-container">
                <div class="footer-left">
                    <div class="tab-listup">
                        <a class="tab" href="http://www.loofre.com/about-us/">About Us</a>
                        <a class="tab" href="http://www.loofre.com/our-team/">Team</a>
                        <a class="tab" href="http://www.loofre.com/contact-us/">Contact Us</a>
                    </div>
                    <div class="tab-listdown">
                        <a class="tab fulltab" href="http://www.loofre.com/merchant/">Add Your Restaurant</a>
                    </div>
                </div>
                <div class="footer-right">
                    <div class="footer-item">
                        <h3><b>For Coverage : </b>Contact@loofre.com | 9599781810</h3>
                        <div id="footer_social"> Join us
                            <a href="https://www.facebook.com/loofreonweb" target="_blank" class="fb"></a>
                            <a href="https://twitter.com/loofre" target="_blank" class="twitter"></a>
                            <a href="https://plus.google.com/102325043925898684671/about" target="_blank" class="gplus"></a>
                            <a href="https://www.youtube.com/channel/UCM55JsUBN2B1BLrJ15xsSjg" target="_blank" class="youtube"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer-bottom">
            <div class="footer-container">
                <div id="copy_right">
                    Copyright © 2015 K5 Brand Solutions, All rights reserved :
                    <a href="http://www.loofre.com/privacy-policy">Privacy Policy </a>
                    <span>/</span>
                    <a href="http://www.loofre.com/terms-of-use/">Terms of Use</a>
                    <span>/</span>
                    <a href="http://www.loofre.com/sitemap.xml">Sitemap</a>
                    <span>/</span>
                </div>
                <div id="designed_by">
                    <span>Designed &amp; Developed by : K5 Brand Solutions</span>
                </div>
            </div>
        </div>
        <span id="topBtn" style="display: none;"></span>

    </footer>
</div>	
</body>

</html>

@endsection 
