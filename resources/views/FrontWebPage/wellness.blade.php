@extends('layouts.default_page')
@section('content')
	<div id="page-top">

		<div class="container">
			<div class="col-lg-12 main-content listing" id="vendor_list">
			</div>
		</div>

		<div id="unlockCode" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="col-xs-12 text-center">
					<p class="modal-title logo "><img src="" alt=""/></p>
					<h6 class="modal-title">Offer valid only on <span id="sublier_name"></span></h6>
				</div>	
			  </div>
			  <div class="modal-body">
				<div class="">
					<!--<p class="disInfo">Kindly show to this merchant</p>-->
					<div class="col-xs-12">
						<div class="col-xs-8 disInfo">Coupon Code : <button id="vendor_code" type="button" class="btn btn-success"></button></div>
						<div class="col-xs-4 disInfo">Any Issue : <button id="callbtn" type="button" class="btn btn-danger glyphicon glyphicon-phone disInfo"><a href="tel:+919540164872">Call</a></button></div>
					</div>
					<p class="disInfo">Offer : <span class="discountSec"></span></p>
					<div class="disInfo text-right"><a href="" class="disInfo text-right"> T&C </a></div>
				</div>
			  </div>
			  <div class="modal-footer">
				  	<div class="col-xs-12 text-center">
					<!--<p class="modal-title" id="vendor_logo"><img src="" alt=""/></p> -->
					<h6 class="modal-title">Kindly show to the merchant <br><span id="vender_name"></span></h6>
				</div>
			  </div>
			</div>

		  </div>
		</div>
		@include('FrontWebPage.includes.Booking.dining.dining-booking')
	</div>
<style>

.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 140px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px;
  position: absolute;
  z-index: 1;
  bottom: 150%;
  left: 50%;
  margin-left: -75px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
  opacity: 1;
}


</style>
@endsection 
