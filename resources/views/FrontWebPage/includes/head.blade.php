
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<link rel="stylesheet" href="{{ asset('/admin/bower_components/bootstrap/dist')}}/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('/admin/bower_components/font-awesome')}}/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('/admin/bower_components/Ionicons')}}/css/ionicons.min.css"> 
<link rel="stylesheet" href="{{ asset('/frontEnd/')}}/css/style.css"> 
<script src="{{ asset('/frontEnd')}}/js/jquery.min.js"></script>
<script src="{{ asset('/frontEnd')}}/js/bootstrap.min.js"></script>
<script src="{{ asset('/frontEnd')}}/js/app-custom.js"></script>
<script src="{{ asset('/frontEnd')}}/js/search.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<title>Loofre | Fine Dining App</title>


 



