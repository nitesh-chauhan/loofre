<!-- Modal -->
<div class="modal left fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="sidebarHead">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="sidebarHeadLeft">
						<a href=""><img src="http://www.loofre.co.in/images/frontImage/HomePage/logo.png" /></a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="sidebarHeadRight">
						<p class="uname"></p>
						<p class="contact"></p>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="sidebarBody">
					<div class="list-group" id="sidebarHeadRight">
						<p class="userEmail list-group-item d-flex justify-content-between align-items-center"><span>Email Id:</span><span class="user_email badge badge-primary badge-pill"></span></p>
						<p class="userSC list-group-item d-flex justify-content-between align-items-center"><span>SubCode:</span><span class="user_Sub_code badge badge-primary badge-pill"></span></p>
						<p class="SCExp list-group-item d-flex justify-content-between align-items-center"><span>ExpiryDate:</span><span class=" expiry_date badge badge-primary badge-pill"></span></p>
						<p class=""><button class="btn btn-primary" type="button" id="logout">logout</button></p>

					</div>
				</div>
			</div>

		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- modal -->
