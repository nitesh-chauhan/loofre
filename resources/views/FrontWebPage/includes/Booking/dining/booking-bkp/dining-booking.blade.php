<div id="dining_booking" class="modal fade" role="dialog">
	<div class="modal-dialog modal-md">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="col-xs-12 text-center">
			<!--<p class="modal-title logo "><img src="" alt=""/></p>-->
			<h6 class="modal-title">Dining Booking<span id="sublier_name"></span></h6>
		</div>	
	  </div>
	  <div class="modal-body" style="padding: 0px;">
		<div class="">
			<div class="col-xs-12">
				
				<form name="dining_bookingForm" role="form" id="dining_bookingForm">
					<p id="msg"></p>
					<input  style="display:none;" type="text" class="form-control" id="registerUsername" name="registerUsername" value="" required="" >
					<input  style="display:none;" type="text" class="form-control" id="registerUserId"name="user_id" value="" required="" >
					<input style="display:none;" type="text" class="form-control" id="restaurants_id" name="restaurants_id" value="" required="">
					<input style="display:none;" type="text" class="form-control" id="offer" name="offer" value="" required="">

					<div class="form-row">
						<div class="form-group col-xs-6">
							<input type="text" class="form-control" id="datepicker" name="booking_date" value="" required="" placeholder="Select Date">
						</div>
						<div class="form-group col-xs-6">
							<select name="booking_time" id="booking_time" class="sel form-control" title="Time" required="">
								<option value="" selected="selected">Select Time</option>
								<option value="00:00">12:00 AM</option>
								<option value="00:30">12:30 AM</option>
								<option value="01:00">01:00 AM</option>
								<option value="01:30">01:30 AM</option>
								<option value="02:00">02:00 AM</option>
								<option value="02:30">02:30 AM</option>
								<option value="03:00">03:00 AM</option>
								<option value="03:30">03:30 AM</option>
								<option value="04:00">04:00 AM</option>
								<option value="04:30">04:30 AM</option>
								<option value="05:00">05:00 AM</option>
								<option value="05:30">05:30 AM</option>
								<option value="06:00">06:00 AM</option>
								<option value="06:30">06:30 AM</option>
								<option value="07:00">07:00 AM</option>
								<option value="07:30">07:30 AM</option>
								<option value="08:00">08:00 AM</option>
								<option value="08:30">08:30 AM</option>
								<option value="09:00">09:00 AM</option>
								<option value="09:30">09:30 AM</option>
								<option value="10:00">10:00 AM</option>
								<option value="10:30">10:30 AM</option>
								<option value="11:00">11:00 AM</option>
								<option value="11:30">11:30 AM</option>
								<option value="12:00">12:00 PM</option>
								<option value="12:30">12:30 PM</option>
								<option value="13:00">01:00 PM</option>
								<option value="13:30">01:30 PM</option>
								<option value="14:00">02:00 PM</option>
								<option value="14:30">02:30 PM</option>
								<option value="15:00">03:00 PM</option>
								<option value="15:30">03:30 PM</option>
								<option value="16:00">04:00 PM</option>
								<option value="16:30">04:30 PM</option>
								<option value="17:00">05:00 PM</option>
								<option value="17:30">05:30 PM</option>
								<option value="18:00">06:00 PM</option>
								<option value="18:30">06:30 PM</option>
								<option value="19:00">07:00 PM</option>
								<option value="19:30">07:30 PM</option>
								<option value="20:00">08:00 PM</option>
								<option value="20:30">08:30 PM</option>
								<option value="21:00">09:00 PM</option>
								<option value="21:30">09:30 PM</option>
								<option value="22:00">10:00 PM</option>
								<option value="22:30">10:30 PM</option>
								<option value="23:00">11:00 PM</option>
								<option value="23:30">11:30 PM</option>
							</select>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-group col-xs-6">
							<select name="no_gust" id="no_gust" class="sel form-control" title="Guest" required="">
								<option value="" selected="selected">Number of Guest</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
						<div class="form-group col-xs-6">
							<select name="no_kids" id="no_kids" class="sel form-control" title="Kids" required="">
								<option value="" selected="selected">Number of Kids</option>
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
					</div>

					<div class="form-group ">
						<label for="email">
							Email:</label>
						<input type="email" class="form-control" id="email" name="email" value="" required="" maxlength="50">
					</div>
					<div class="form-group">
						<label for="mobile">
							Mobile:</label>
						<input type="mobile" class="form-control" id="mobile" name="mobile" value="" required="" maxlength="10">
					</div>
					

					<button  class="btn-lg btn-success btn-block" id="bookDBtn">Submit</button>
				</form>
				<div id="con">
				
				</div>
			</div>
		</div>
	  </div>
	  <div class="modal-footer">
			<div class="col-xs-12 text-center">
		</div>
	  </div>
	</div>

	</div>
</div>




<script>


function bookDiningOutlet(venderOutletsId,discount){
	//$("#con").html('');
	//$("#dining_bookingForm").show(); 
	$("#dining_bookingForm").trigger("reset");
	var userId = localStorage.getItem('userId');
	var name = localStorage.getItem('name');
	var email = localStorage.getItem('email');
	var mobile = localStorage.getItem('mobile');
	var supplier_id = localStorage.getItem('supplier_id');
	var Suplier_aliasName = localStorage.getItem('aliasName');
	$("#registerUserId").val(userId);
	$("#restaurants_id").val(venderOutletsId);
	$("#offer").val(discount);
	$("#registerUsername").val(name);
	$("#email").val(email);
	$("#mobile").val(mobile);
	
	}
	
	
function getConfirmationdb(values){
	$("#dining_bookingForm").hide(); 
	var userName = $("#registerUsername").val();
	var userEmail = $("#email").val();
	var userMobile = $("#mobile").val();
	var bookingDate = $("#datepicker").val();
	var bookingTime = $("#booking_time").val();
	var offer = $("#offer").val();
	var guest = $("#no_gust").val();
	var kids = $("#no_kids").val();
	alert(userEmail);
	
//-------prepare confirmation div and append into Start	---------------
	var confirmDiv='';
	 confirmDiv += '<div class="col-xs-12">';
		confirmDiv += '<div class="col-xs-12">';
			confirmDiv += '<div class="col-xs-6" id="name_conf"><span>Name : <span>'+userName+'</div>';
			confirmDiv += '<div class="col-xs-6" id="email_conf"><span>Email : <span>'+userEmail+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12">';
			confirmDiv += '<div class="col-xs-6" id="mobile_conf"><span>Mobile : <span>'+userMobile+'</div>';
			confirmDiv += '<div class="col-xs-6" id="offer_conf"><span>Offer : <span>'+offer+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12">';
			confirmDiv += '<div class="col-xs-6" id="date_conf"><span>Booking Date : <span>'+bookingDate+'</div>';
			confirmDiv += '<div class="col-xs-6" id="time_conf"><span>Booking Time : <span>'+bookingTime+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12">';
			confirmDiv += '<div class="col-xs-6" id="guest_conf"><span>Guest : <span>'+guest+'</div>';
			confirmDiv += '<div class="col-xs-6" id="Kids_conf"><span>Kids : <span>'+kids+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12">';
			confirmDiv += '<button type="btn" class="col-xs-6 btn-sm btn-success" id="replanDB">Replan</button>';
			confirmDiv += '<button type="btn" class="col-xs-6 btn-sm btn-danger" id="confirmDB">Confirm</button>';
		confirmDiv += '</div>';
	confirmDiv += '</div>';
				
	$("#con").html(confirmDiv);
//-------prepare confirmation div and append into End---------	
//-------Replan Start---------	
	
	$("#replanDB").on('click',function(){
		$("#con").html('');
		$("#dining_bookingForm").show();
		});
//-------Replan End---------	
//-------Confirm and book Start---------	
	
	$("#confirmDB").on('click',function(){
		$("#con").html('<span style="color:Green">Your Bookinjg Confirm. Thank You!</span>');
		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/booking-dining",
			dataType:"json",
			data: $("#dining_bookingForm").serialize(),//only input
			success: function(data){
				if(data.status=='error'){
					//alert(data.msg);
					$("#msg").html('<span style="color:red">'+data.msg+'</span>');
				}else{ 
				//console.log(values);
				setTimeout(function(){ location.reload() }, 1000);
					
				}
			}
		   
		});

	});	
	
//-------Confirm and book Start---------	
	

	}	
	

</script>

<script type="text/javascript">
$("#dining_bookingForm").on( "submit", function( event ) {
	var values = $(this).serializeArray();
	getConfirmationdb(values);
	event.preventDefault();
});
</script>

<style>
#dining_booking .form-group {
    padding: 2px;
}

</style>
