<div id="buffet_booking" class="modal fade" role="dialog">
	<div class="modal-dialog modal-md">
	<!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="col-xs-12 text-center">
			<!--<p class="modal-title logo "><img src="" alt=""/></p>-->
			<h6 class="modal-title">Buffet Booking<span id="sublier_name"></span></h6>
		</div>	
	  </div>
	  <div class="modal-body" style="padding: 0px;">
		<div class="">
			<div class="">
				<div id="loggedInUser">Dear <span></span>,</div>
				<form name="buffet_bookingForm" role="form" id="buffet_bookingForm">
					<div class="form-group ">
						<input type="email" class="form-control" id="email" name="email" value="" required="" maxlength="50">
					</div>
					<div class="form-group">
						<input type="mobile" class="form-control" id="mobile" name="mobile" value="" required="" maxlength="10">
					</div>
					<div class="form-group ">
						<div id="buffetListMenu">
							<ul class="threeCol"></ul>		
						</div>
						<div id="timewiseBuffetList">
							<div id="BreakfastBuffetList" class="buffetList" style="display: none;">
								<ul></ul>
							</div>
							<div id="LunchBuffetList" class="buffetList" style="display: none;">
								<ul></ul>
							</div>
							<div id="DinnerBuffetList" class="buffetList" style="display: none;">
								<ul></ul>
							</div>
							<div id="BrunchBuffetList" class="buffetList" style="display: none;">
								<ul></ul>
							</div>
						</div>
					</div>
					<div class="form-group" id="buffetCart">
					</div>
					<input style="display:none;" type="text" class="form-control" id="suplier_id" name="suplier_id" value="" required="" >
					<input style="display:none;" type="text" class="form-control" id="registerUsername" name="registerUsername" value="" required="" >
					<input style="display:none;" type="text" class="form-control" id="registerUserId"name="user_id" value="" required="" >
					<input style="display:none;" type="text" class="form-control" id="restaurants_name" name="restaurants_name" value="" required="">
					<input style="display:none;" type="text" class="form-control" id="restaurants_id" name="restaurants_id" value="" required="">
					<!--<input style="display:none;" type="text" class="form-control" id="offer" name="offer" value="" required="">
					-->
					<div class="form-row" id="dataAndTime">
						<div class="form-group col-xs-6">
							<input type="text" class="form-control booking_date" id="datepicker" name="booking_date" value="" required="" placeholder="Select Date">
						</div>
						<div class="form-group col-xs-6">
							<select name="booking_time" id="booking_time" class="sel form-control" title="Time" required="">
								<option class="" value="" selected="selected">Select Time</option>
								<option class="availabletime" value="09:00 AM">9:00 AM</option>
								<option class="availabletime" value="09:30 AM">09:30 AM</option>
								<option class="availabletime" value="10:00 AM">10:00 AM</option>
								<option class="availabletime" value="10:30 AM">10:30 AM</option>
								<option class="availabletime" value="11:00 AM">11:00 AM</option>
								<option class="availabletime" value="11:30 AM">11:30 AM</option>
								<option class="availabletime" value="12:00 PM">12:00 PM</option>
								<option class="availabletime" value="12:30 PM">12:30 PM</option>
								<option class="availabletime" value="13:00 PM">01:00 PM</option>
								<option class="availabletime" value="13:30 PM">01:30 PM</option>
								<option class="availabletime" value="14:00 PM">02:00 PM</option>
								<option class="availabletime" value="14:30 PM">02:30 PM</option>
								<option class="availabletime" value="15:00 PM">03:00 PM</option>
								<option class="availabletime" value="15:30 PM">03:30 PM</option>
								<option class="availabletime" value="16:00 PM">04:00 PM</option>
								<option class="availabletime" value="16:30 PM">04:30 PM</option>
								<option class="availabletime" value="17:00 PM">05:00 PM</option>
								<option class="availabletime" value="17:30 PM">05:30 PM</option>
								<option class="availabletime" value="18:00 PM">06:00 PM</option>
								<option class="availabletime" value="18:30 PM">06:30 PM</option>
								<option class="availabletime" value="19:00 PM">07:00 PM</option>
								<option class="availabletime" value="19:30 PM">07:30 PM</option>
								<option class="availabletime" value="20:00 PM">08:00 PM</option>
								<option class="availabletime" value="20:30 PM">08:30 PM</option>
								<option class="availabletime" value="21:00 PM">09:00 PM</option>
								<option class="availabletime" value="21:30 PM">09:30 PM</option>
								<option class="availabletime" value="22:00 PM">10:00 PM</option>
								<option class="availabletime" value="22:30 PM">10:30 PM</option>
								<option class="availabletime" value="23:00 PM">11:00 PM</option>
								<option class="availabletime" value="23:30 PM">11:30 PM</option>
								<option class="availabletime" value="24:00 PM">12:00 AM(midnight)</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12" id="availableBuffetDiscounts" >
						<span id="discountTitle">Loofre Discount</span>
						<div class="buffet_discount">
							<input type="radio" value="" id="offer" name="offer" checked="">
							<label for="loofreDiscount1">10% off on Buffet	</label>
						</div>
					</div>
					<button  class="btn-lg btn-success btn-block" id="bookDBtn">Submit</button>
				</form>
				<div id="con">
				</div>
			</div>
		</div>
	  </div>
	  <div class="modal-footer--">
			<div class="col-xs-12 text-center">
			<button  class="btn-xs btn-danger" id="close">Close</button>
	
			</div>
	  </div>
	</div>

	</div>
</div>
<span id="buffetTiming" style="display:none"></span>
<div id="buffetCardTemplate" style="display: none;">
	<div class="buffetCard">
		<div class="buffetCardTitle"></div>
		<div class="buffetCardMid"></div>
		<div class="buffetCardFooter">
			<div class="buffetCardQuantity">
				<input class="buffet_id" name="buffet_id" type="text">
				<input class="no_buffet" name="no_buffet" type="text">
				<input class="buffetQuantity" type="text">
			</div>
			<div class="buffetCardPrice"></div>
		</div>
	</div>
</div>

<form name="selectedBuffetForm" role="form" id="selectedBuffetForm" style="display:none;">

</form>
<div id="bcart"></div>


<script>


function bookBuffetOutlet(venderOutletsId,discount){
	$("#buffet_bookingForm").trigger("reset");
	var discount = ""+discount+" % off on Buffet";
	var userId = localStorage.getItem('userId');
	var name = localStorage.getItem('name');
	var email = localStorage.getItem('email');
	var mobile = localStorage.getItem('mobile');
	var supplier_id = localStorage.getItem('supplier_id');
	var Suplier_aliasName = localStorage.getItem('aliasName');
	$("#registerUserId").val(userId);
	$("#restaurants_id").val(venderOutletsId);
	$("#suplier_id").val(supplier_id);
	$("#registerUsername").val(name);
	$("#loggedInUser span").html(name);
	$("#email").val(email);
	$("#mobile").val(mobile);
	$(".buffet_discount input").val(discount);
	$(".buffet_discount label").html(discount);
	
	$('#datepicker').datepicker({ 
		dateFormat: 'yy/mm/dd',
		hideIfNoPrevNext: true,
		minDate: '-0',
		maxDate: '+7D' });
    $('#datepicker').datepicker('setDate', new Date());
	$("#selectedBuffetForm").html("");
	$("#dataAndTime").hide();
	getBuffetItemList(venderOutletsId);
	getOutletData(venderOutletsId);
	$("#BreakfastBuffetList,#LunchBuffetList,#DinnerBuffetList,#BrunchBuffetList").html('');

	}


function getOutletData(venderOutletsId){
	var userId = localStorage.getItem('userId');
	var supplier_id = localStorage.getItem('supplier_id');
	var Suplier_aliasName = localStorage.getItem('aliasName');
	$.ajax({
		type: "post",
		url: "http://www.loofre.co.in/api/restaurant-coupon",
		dataType:"json",
		data: {userId:userId, venderOutletsId:venderOutletsId, supplier_id:supplier_id, aliasName:Suplier_aliasName},
		success: function(data){			
			$("#restaurants_name").val(data.outletName);
			//$("#offer").val(data.discount);
		}
	});
	
	}

	
function getConfirmationdb(values){
	$("#buffet_bookingForm").hide(); 
	var userName = $("#registerUsername").val();
	var userEmail = $("#email").val();
	var userMobile = $("#mobile").val();
	var bookingDate = $("#datepicker").val();
	var bookingTime = $("#booking_time").val();
	var buffetName = $("#selectedBuffetForm .buffetCardTitle").html();
	var buffetQuantity = $("#selectedBuffetForm .buffetQuantity").val();
	var restaurants_name = $("#restaurants_name").val();
	var offer = $("#offer").val();
	$("#bcart").html("");
	$("#selectedBuffetForm .buffetCard").each(function(){
	var title = $(this).find(".buffetCardTitle").html();
	var quantity = $(this).find(".buffetQuantity").val();
	cartDiv = '<div class="col-xs-12" id="guest_conf">'+title+',('+quantity+')</div>';
	$("#bcart").append(cartDiv);
		});
	var cartList = $("#bcart").html();

//-------prepare confirmation div and append into Start	---------------
	var confirmDiv='';
	 confirmDiv += '<div class="col-xs-12 booking-list">';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<div class="col-xs-12" id="mobile_conf"><span>Mobile : </span>'+userMobile+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<div class="col-xs-12" id="email_conf"><span>Email : </span>'+userEmail+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<div class="col-xs-12" id="offer_conf"><span>Offer : </span>'+offer+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<div class="col-xs-12" id="date_conf"><span>Booking Date : </span>'+bookingDate+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<div class="col-xs-12" id="time_conf"><span>Booking Time : </span>'+bookingTime+'</div>';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<span>Buffet List  : </span>'+cartList+'';
		confirmDiv += '</div>';
		confirmDiv += '<div class="col-xs-12 booking-listrow">';
			confirmDiv += '<button type="btn" class="col-xs-6 btn-sm btn-success" id="replanDB">Replan</button>';
			confirmDiv += '<button type="btn" class="col-xs-6 btn-sm btn-danger" id="confirmBB">Confirm</button>';
		confirmDiv += '</div>';
	confirmDiv += '</div>';
				
	$("#con").html(confirmDiv);
//-------prepare confirmation div and append into End---------	
//-------Replan Start---------	
	
	$("#replanDB").on('click',function(){
		$("#con").html('');
		$("#buffet_bookingForm").show();
		});
//-------Replan End---------	
//-------Confirm and book Start---------	
	
	$("#confirmBB").on('click',function(){
				var ThanksDiv='';
		 ThanksDiv += '<div class="col-xs-12 booking-list">';
			ThanksDiv += '<div class="col-xs-12">';
				ThanksDiv += '<p>Dear '+userName+',</p>';
				ThanksDiv += '<p>We have received your request for the reservation in '+restaurants_name+' on '+bookingDate+' at '+bookingTime+'</p>';
				ThanksDiv += '<p>Soon you will get the confirmation call from our team</p>';
				ThanksDiv += '<p>Have a great time !!</p>';
				ThanksDiv += '<p>Team Loofre </p>';
			ThanksDiv += '</div>';
		ThanksDiv += '</div>';				
		$("#con").html(ThanksDiv);
		
		$("#close").show();
		$("#close").on('click', function(){
			location.reload(true);
			});
			console.log(values);

		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/booking-buffet",
			dataType:"json",
			data: $("#buffet_bookingForm, #selectedBuffetForm").serialize(),//only input
			success: function(data){
				if(data.status=='error'){	
					alert(data.msg);
					$("#msg").html('<span style="color:red">'+data.msg+'</span>');
				}else{ 
					
					console.log(data.status);
				    console.log(data);
				}
			}
		   
		});
	});	

//-------Confirm and book Start---------	
	

	}	
	
	
//-------buffet List start---------	


function getActivebuffetListMenu(buffetCount){
	$("#buffetListMenu ul li").first().removeClass().addClass('selected');
	var id = $("#buffetListMenu ul li").attr('data-target');
	    buffetTimingId = "#"+id+"";
	    $(buffetTimingId).show();
	    $(buffetTimingId).siblings().hide();

	$('#buffetListMenu ul li').on('click', function() {		
		var selectedBuffetNum = 0;
		var currentSelElem = $("#buffetListMenu ul li.selected");
		$("#" + currentSelElem.data("target") + " .serviceQuantity").each(function(index, elem) {
			selectedBuffetNum += parseInt($(elem).val());
		});
		if(selectedBuffetNum) {
			alert("You have already selected  " + currentSelElem.html());
		}else{
			
			var buffetType = $(this).html();
			$(this).addClass('selected').siblings().removeClass('selected');
			var id = $(this).attr('data-target');
			buffetTimingId = "#"+id+"";
			$(buffetTimingId).siblings().hide();
			$(buffetTimingId).show();
			
			}

		
	});
	
/* Show active Menu  start*/	
	if(buffetCount==1){
		$("#buffetListMenu ul").removeClass().addClass('oneCol');	
	}else if(buffetCount==2){
		$("#buffetListMenu ul").removeClass().addClass('twoCol');	
	}else if(buffetCount==3){
		$("#buffetListMenu ul").removeClass().addClass('threeCol');	
	}else if(buffetCount==4){
		$("#buffetListMenu ul").removeClass().addClass('fourCol');	
	}
/* Show active Menu End */	
/* increaseQuantity  start */	

$(".increaseQuantity").on("click", function(e) {
		var buffetId = $(this).parents(".buffetItemView").find(".buffetTitle").attr('id').replace("buffet_", "");
		$("#buffetTiming").html($(this).parents(".buffetItemContainer").find(".startTime").html() + " - " + $(this).parents(".buffetItemContainer").find(".endTime").html());
		var buffetTitle = $(this).parents(".buffetItemView").find(".buffetTitle").html();
		var price = $(this).parents(".buffetItemView").find(".buffetPrice").html();
		var quantity = parseInt($(this).parent().find(".serviceQuantity").val()) + 1;
		$(this).parent().find(".serviceQuantity").val(quantity);
		redrawTimingSheet();
		bookBuffet(buffetId, buffetTitle, price, quantity);
		$("#dataAndTime").fadeIn();
	
});	


/* increaseQuantity End */	
/* decreaseQuantity start */	
$(".decreaseQuantity").on("click", function(e) {

		var buffetId = $(this).parents(".buffetItemView").find(".buffetTitle").attr('id').replace("buffet_", "");
		$("#buffetTiming").html($(this).parents(".buffetItemContainer").find(".startTime").html() + " - " + $(this).parents(".buffetItemContainer").find(".endTime").html());
		var buffetTitle = $(this).parents('.buffetItemView').find(".buffetTitle").html();
		var price = $(this).parents(".buffetItemView").find(".buffetPrice").html();
		redrawTimingSheet();
		var quantity = parseInt($(this).parent().find(".serviceQuantity").val()) - 1;
		if(quantity >= 1) {
		//setItemsAndTimings($(this).parents(".buffetItemContainer").find(".startTime").html(), $(this).parents(".buffetItemContainer").find(".endTime").html());
			$(this).parent().find(".serviceQuantity").val(quantity);
			bookBuffet(buffetId, buffetTitle, price, quantity);
		} else if(quantity == 0) {
			$(this).parent().find(".serviceQuantity").val(quantity);
			$("#buffetCard_" + buffetId).remove();
		}
		if($("#selectedBuffetForm .buffetCard").length == 0) {
			$("#dataAndTime").fadeOut();
		}

});

/* decreaseQuantity end */	

}
	
function getBuffetItemList(venderOutletsId){
		
		$.ajax({
			type: "post",
			url: "http://www.loofre.co.in/api/buffet-services",
			dataType: "json",
			data: {venderOutletsId:venderOutletsId},
			success: function(data){
				if(data.status=='error'){
				}else{ 			
				console.log(data);
				var inputArray = [];
				for (var i = 0; i < data["VendorBuffet"].length; i++) {
						var tt = data["VendorBuffet"][i].timingType;
						if(tt=="Breakfast"){
							inputArray.push(data["VendorBuffet"][i].timingType);
							var buffetItems = '';
							buffetItems += '<li class="buffetItemContainer">',
							buffetItems += 		'<div class="buffetItemView" title="Select">',
							buffetItems += 			'<div id="buffet_'+data["VendorBuffet"][i].outletBuffetId+'" class="buffetTitle">'+data["VendorBuffet"][i].buffetTitle+'<span class="buffetPrice"> (INR '+data["VendorBuffet"][i].price+')</span></div>',
							buffetItems += 			'<div class="serviceCardQuantity">',
							buffetItems += 				'<span class="decreaseQuantity">-</span>',
							buffetItems += 				'<input disabled="" name="serviceQuantity" class="serviceQuantity" value="0" type="text">',
							buffetItems += 				'<span class="increaseQuantity">+</span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 		'<div class="buffetDetail" style="display:none">',
							buffetItems += 			'<div class="buffetDetailTiming">',
							buffetItems += 			'<b>Timing : </b><span class="buffetTiming"><span class="startTime">'+data["VendorBuffet"][i].startTime+'</span> to <span class="endTime">'+data["VendorBuffet"][i].endTime+'</span></span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 	'</li>';
							$("#BreakfastBuffetList").append(buffetItems);
						
						}
						else if(tt=="Lunch"){
							inputArray.push(data["VendorBuffet"][i].timingType);
							var buffetItems = '';
							buffetItems += '<li class="buffetItemContainer">',
							buffetItems += 		'<div class="buffetItemView" title="Select">',
							buffetItems += 			'<div id="buffet_'+data["VendorBuffet"][i].outletBuffetId+'" class="buffetTitle">'+data["VendorBuffet"][i].buffetTitle+'<span class="buffetPrice"> (INR '+data["VendorBuffet"][i].price+')</span></div>',
							buffetItems += 			'<div class="serviceCardQuantity">',
							buffetItems += 				'<span class="decreaseQuantity">-</span>',
							buffetItems += 				'<input disabled="" name="serviceQuantity" class="serviceQuantity" value="0" type="text">',
							buffetItems += 				'<span class="increaseQuantity">+</span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 		'<div class="buffetDetail" style="display:none">',
							buffetItems += 			'<div class="buffetDetailTiming">',
							buffetItems += 			'<b>Timing : </b><span class="buffetTiming"><span class="startTime">'+data["VendorBuffet"][i].startTime+'</span> to <span class="endTime">'+data["VendorBuffet"][i].endTime+'</span></span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 	'</li>';
							$("#LunchBuffetList").append(buffetItems);
						}
						else if(tt=="Dinner"){
							inputArray.push(data["VendorBuffet"][i].timingType);
							var buffetItems = '';
							buffetItems += '<li class="buffetItemContainer">',
							buffetItems += 		'<div class="buffetItemView" title="Select">',
							buffetItems += 			'<div id="buffet_'+data["VendorBuffet"][i].outletBuffetId+'" class="buffetTitle">'+data["VendorBuffet"][i].buffetTitle+'<span class="buffetPrice"> (INR '+data["VendorBuffet"][i].price+')</span></div>',
							buffetItems += 			'<div class="serviceCardQuantity">',
							buffetItems += 				'<span class="decreaseQuantity">-</span>',
							buffetItems += 				'<input disabled="" name="serviceQuantity" class="serviceQuantity" value="0" type="text">',
							buffetItems += 				'<span class="increaseQuantity">+</span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 		'<div class="buffetDetail" style="display:none">',
							buffetItems += 			'<div class="buffetDetailTiming">',
							buffetItems += 			'<b>Timing : </b><span class="buffetTiming"><span class="startTime">'+data["VendorBuffet"][i].startTime+'</span> to <span class="endTime">'+data["VendorBuffet"][i].endTime+'</span></span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 	'</li>';
							$("#DinnerBuffetList").append(buffetItems);
						}
						else if(tt=="Brunch"){
							inputArray.push(data["VendorBuffet"][i].timingType);
							var buffetItems = '';
							buffetItems += '<li class="buffetItemContainer">',
							buffetItems += 		'<div class="buffetItemView" title="Select">',
							buffetItems += 			'<div id="buffet_'+data["VendorBuffet"][i].outletBuffetId+'" class="buffetTitle">'+data["VendorBuffet"][i].buffetTitle+'<span class="buffetPrice"> (INR '+data["VendorBuffet"][i].price+')</span></div>',
							buffetItems += 			'<div class="serviceCardQuantity">',
							buffetItems += 				'<span class="decreaseQuantity">-</span>',
							buffetItems += 				'<input disabled="" name="serviceQuantity" class="serviceQuantity" value="0" type="text">',
							buffetItems += 				'<span class="increaseQuantity">+</span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 		'<div class="buffetDetail" style="display:none">',
							buffetItems += 			'<div class="buffetDetailTiming">',
							buffetItems += 			'<b>Timing : </b><span class="buffetTiming"><span class="startTime">'+data["VendorBuffet"][i].startTime+'</span> to <span class="endTime">'+data["VendorBuffet"][i].endTime+'</span></span>',
							buffetItems += 			'</div>',
							buffetItems += 		'</div>',
							buffetItems += 	'</li>';
							$("#BrunchBuffetList").append(buffetItems);
						}
				
				}
					var buffetTitle = array_unique(inputArray);
					var buffetHeader = ''; var buffetCount='';
					for (var i = 0; i < buffetTitle.length; i++) {
						buffetHeader += '<li data-target="'+buffetTitle[i]+'BuffetList">'+buffetTitle[i]+'</li>';
						buffetCount++;
					}
					$("#buffetListMenu ul").html(buffetHeader);
					
					getActivebuffetListMenu(buffetCount);

				
				}
				
				}
			
			
			});
			
		}
		
function array_unique(inputArray) {
    var unique = [];
    for ( var i = 0 ; i < inputArray.length ; ++i ) {
        if ( unique.indexOf(inputArray[i]) == -1 )
            unique.push(inputArray[i]);
    }
    return unique;
}
function bookBuffet(buffetId, buffetTitle, price, quantity) {
	if((buffetId == undefined) || (buffetTitle == undefined) || (price == undefined)) {
		console.log("Bad buffet booked.");
	} else {
		var bookedBuffet = $("#buffet_" + buffetId).parents(".buffetItemContainer");
		 bookedBuffet.find(".serviceQuantity").val(quantity);
		 //bookedBuffet.find(".buffet_id").val(buffetId);
		if($("#selectedBuffetForm #buffetCard_" + buffetId).length == 0) {
			if(quantity == undefined) {
				quantity = 1;
			}
			var buffetCard = $(document.getElementById("buffetCardTemplate").children[0].cloneNode(true));
			var input = buffetCard.find(".buffetQuantity");
			input.val(quantity).attr("name", "buffet_" + buffetId);
			buffetCard.find(".buffet_id").val(buffetId);
			buffetCard.find(".no_buffet").val(quantity);
			buffetCard.find(".buffetCardTitle").html(buffetTitle);
			buffetCard.attr("id", "buffetCard_" + buffetId);
			buffetCard.find(".buffetCardPrice").html(price);
			$("#selectedBuffetForm").append(buffetCard);
			//generateBill();
		} else {
			$("#buffetCard_" + buffetId).find(".buffetQuantity").val(quantity);
			$("#buffetCard_" + buffetId).find(".no_buffet").val(quantity);
		}
	}
}
	
</script>

<script type="text/javascript">
$("form").on( "submit", function( event ) { 
	var phoneNum = $("#mobile").val();
	if((phoneNum.length != 10) || (parseInt(phoneNum) != phoneNum)) {
		alert("Please fill in a 10 digit mobile number.");
		$("#mobile").focus();
	} else if($(".buffetCard").length < 2) {
		alert("Please select atleast one Buffet.");
	} else if($("#booking_time").val() == "") {
		$("#overlay").css("display", "block");
		$("#booking_time").show();
	} else {
			alert("Not Ready Conf");
			var values = $("#buffet_bookingForm, #selectedBuffetForm").serializeArray();
			//var values = $("#buffet_bookingForm").serializeArray();
			//var BookBooffet = $("#selectedBuffetForm").serializeArray();
		//values.push({name: "BookBooffet", value: BookBooffet});

			
			
			getConfirmationdb(values);
	}
	event.preventDefault();
});

</script>
<script> 
function strToTime(time) {
	//time "8:30 AM"
	var t = {};
	if(time.trim() == "") {
		t.hours = 0;
		t.minutes = 0;
	} else {
		var hours = Number(time.match(/^(\d+)/)[1]);
		var minutes = Number(time.match(/:(\d+)/)[1]);
		//var AMPM = time.match(/\s(.*)$/)[1].toUpperCase();
		//alert(hours);alert(minutes);
		var sHours = hours.toString();
		var sMinutes = minutes.toString();
		if(hours<10) sHours = "0" + sHours;
		if(minutes<10) sMinutes = "0" + sMinutes;
		t.hours = sHours;
		t.minutes = sMinutes;
	}
	//console.log(t);
	return t;
}
function getBuffetTiming() {
	var t = {};
	var timingRange = $("#buffetTiming").html().toUpperCase().split("-");
	//alert(timingRange);
	var startTime = strToTime(timingRange[0].trim());
	t.start = new Date($(".booking_date").val());
	t.start.setHours(startTime.hours);
	t.start.setMinutes(startTime.minutes);
	t.start = t.start.getTime();

	var endTime = strToTime(timingRange[1].trim());
	t.end = new Date($(".booking_date").val());
	t.end.setHours(endTime.hours);
	t.end.setMinutes(endTime.minutes);
	t.end = t.end.getTime();

	return t;
}
function isBookingTimeValid(start, time, end) {
	//console.log("start");
	//console.log(start);
	//console.log("Date.now()");
	//console.log(Date.now());
	//console.log(time.minutes);
	//console.log("end");
	//console.log(end);
	var bookingDate = new Date($(".booking_date").val());
	//console.log($(".booking_date").val());
	bookingDate.setHours(time.hours);
	bookingDate.setMinutes(time.minutes);
	bookingTimestamp = bookingDate.getTime();
	//console.log("bookingTimestamp");
	//console.log(bookingTimestamp);

	ifBookingTimeValid = false;
	if((start <= bookingTimestamp) && (bookingTimestamp <= end) && (bookingTimestamp >= Date.now())) {
		ifBookingTimeValid = true;
	}		

	return ifBookingTimeValid;
}
function redrawTimingSheet() {
	var buffetTiming = getBuffetTiming();
	$("#booking_time .availabletime").each(function() {
		var time = strToTime($(this).val());

		if(isBookingTimeValid(buffetTiming.start, time, buffetTiming.end)) {
			//console.log("valid");
			$(this).show();
			//$(this).addClass("DisplayTime");
			//$(this).removeClass("notAvailableTime");
		} else {
			//console.log("false");
			$(this).hide();
			//$(this).addClass("notAvailableTime");
			//$(this).removeClass("DisplayTime");
			//$(this).attr("onclick", null);
		}
	});
}
$(".booking_date").on("change", function(e) {
	redrawTimingSheet();
});
</script>
<style>
#con{overflow: hidden;}	
#dataAndTime{display:none;}	
.availabletime{display:none;}
#buffet_booking{margin-bottom: 62px;}
#buffet_booking .form-group {
    padding: 2px;
}
.booking-listrow{border-bottom: 1px solid; padding: 8px 0px;}
.booking-listrow span{    padding-bottom: 5px;
    color: #676767;
    font: 14px "texgyreherosregular";
    font-weight: 600;
    text-transform: uppercase;
    }
#close{display:none;}
#buffet_booking .modal-dialog{border: 5px solid #ca292e;}
#buffet_booking .modal-title{color: #ca292e;font-weight: 600;font-size: 16px;}
#buffet_booking .close{color: #ca292e;opacity: 1;}
#bookDBtn{background: #5190d7 !important;border: none;}
#availableBuffetDiscounts{background: #e20f24;color:#fff;padding:5px;}
#loggedInUser{    border-bottom: 2px solid #666;
    width: 90%;
    margin: 25px auto 0 auto;
    padding: 3px 0;
    display: block;
 }
#discountTitle{font-size: 16px;font-weight: 600;}
#loggedInUser span{font-weight: bold;color: #ca292e;}

#buffet_bookingForm #email, #buffet_bookingForm #mobile{
    display: block;
    width: 80%;
    height: 25px;
    line-height: 21px;
    margin: 1px auto;
    border: none;
    border-bottom: 1px solid #333;
    text-align: center;
}
#buffet_bookingForm #email {
    background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAJ1BMVEX///93d3d9fX18fHzs7Ox+fn7e3t5sbGzk5ORpaWlVVVXw8PDv7++8evOsAAAAVklEQVQYlYWOSxbAIAgDIwiieP/ztupT+1mYXQYIAY4qOTyUCxLHPY2cQLZJZCMQjOvwlQwNQMWbd1EMMEj3E7C4Cy9wL2gIuk52KPfQ79t3MUr/6kddG2QCF9BSPjIAAAAASUVORK5CYII=") left center no-repeat
}
#buffet_bookingForm #mobile {
    background: url(https://www.loofre.com/wp-content/themes/loofre/images/small-phone-icon.png) left center no-repeat;
}
#buffetListMenu ul{width:100%;float:left;overflow: hidden; border-bottom: 2px solid #0098d6;}
#buffetListMenu ul.oneCol li {width: 100%;}
#buffetListMenu ul.twoCol li {width: 50%;}
#buffetListMenu ul.threeCol li {width: 33.33%;}
#buffetListMenu ul.fourCol li {width: 25%;}
#buffetListMenu ul li.selected {background: #0098d6; color: #fff;}
#buffetListMenu ul li {float: left;text-align: center;line-height: 30px;
    height: 30px;
    cursor: pointer;
    border-radius: 3px 3px 0 0;
}
#timewiseBuffetList ul {overflow: hidden;}
#timewiseBuffetList .buffetItemContainer {padding-left: 10px;height: 35px; line-height: 35px;}
.buffetDetail {display: none;font-size: 0.9em;line-height: 1.6em;padding: 15px;background: #f9f9f9;}
.buffetTitle {float: left; width: 70%;}
#timewiseBuffetList .serviceCardQuantity {float: left;margin-top: 5px;}
#timewiseBuffetList .serviceCardQuantity .decreaseQuantity { background: #ED2623; float: left;
	 width: 20px;
    text-align: center;
    height: 20px;
    line-height: 20px;
    cursor: pointer;
    user-select: none;
    color: #fff;
    border-radius: 3px;}
#timewiseBuffetList .serviceQuantity {
    height: 20px;
    border: none;
    background: #fff;
    line-height: 20px;
    width: 30px;
    text-align: center;
    margin: 0;
    float: left;
}
#timewiseBuffetList .serviceCardQuantity .increaseQuantity {
    background: #66D852;
    float: left;
    width: 20px;
    text-align: center;
    height: 20px;
    line-height: 20px;
    cursor: pointer;
    user-select: none;
    color: #fff;
    border-radius: 3px;
}

</style>
