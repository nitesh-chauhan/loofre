<span id="topBtn"></span>
<div class="mobile_tab" style="z-index: 999999999;position: fixed;top: auto; bottom: 0px;">
		<div class="scroll" id="menuBottom">	
	
		</div>
	</div>
<style>
	
#backbtn{float:right; cursor: pointer; color: #ed3237;}	
.loader{float: right;
margin: -33px 0px 0px 0px;
display:none;
}	
.mobile_tab{    overflow: auto;
    /* border-top: 1px solid #eee; */
    border-bottom: 1px solid #eee; 
    width: 100%;
    height: auto;
    margin: 4px 0px 0px 0px;
    padding: 4px;
    background: #fff;
    font-size: 12px;
    /* box-shadow: 0px -11px 42px 4px inset; */}
	
.mobile_tab .scroll .m_item{    float: left;
    height: 42px;
    width: 42px;
    display: inline-block;
    text-align: center;
    line-height: 40px;
    border-radius: 53%;
    padding: 0px;
    margin: 0px 7px;
    color: #fff;
    vertical-align: middle;
    border-top: 2px solid #f58634;
    border-bottom: 2px solid #f58634;
    border-right: 2px solid #ed3237;
    border-left: 2px solid #0098da;
	
	}
	

.scroll {
  float:left;
	margin:0 -32767px 0 0;/* browser limit */
    white-space: nowrap;
    overflow-y: hidden;
    overflow-x: scroll;
    -webkit-overflow-scrolling: touch;}
 
 .mobile_tab::-webkit-scrollbar {
  display: none;
}
.scroll .m_item img{width:100%;padding:13px 0px;}
.m_item a{color:#fff;	}
.m_item:last-child{margin: 0px 16px;}
 #menuBottom .m_item:last-child{margin: 0px 16px;}
.mobile_tab{display:none;}
.m-box{
    float: left;
    overflow: hidden;
    width: 74px;
}
.cat_name{ float: left;width: 100%; text-align: center;  padding: 2px 0px;}
.cat_name a{color:#777;}
.m_logo{display:none;}
@media only screen and (max-width: 780px){
	.header-top{display:none;}
	#menu_box{display:none;}
	.mobile_tab{display:block;}
    .loofre_logo{display:none;}
	#all_content.boxed_width{max-width:100%!important;}
	.m_logo{display:block;  width: 100%; border-bottom: 1px solid #eee;}
	.m_logo .m_logo_box{padding: 0 30%;}

}
@media only screen and (max-width: 478px){
	#menu_box{display:none;}
	.mobile_tab{display:block;}
    .loofre_logo{display:none;}
	.m_logo{display:block;  width: 100%; border-bottom: 1px solid #eee;}
	.m_logo .m_logo_box{padding: 0 29%;}

}
@media only screen and (max-width: 320px){
	#menu_box{display:none;}
	.mobile_tab{display:block;}
    .loofre_logo{display:none;}
	.m_logo{display:block;  width: 100%; border-bottom: 1px solid #eee;}
	.m_logo .m_logo_box{padding: 0 20%;}

}
main_content > * {
    -webkit-box-sizing: none!important;

}



.r-field{display:none;}
#alradyAcc{display:none;}
#alradyAccTxt{text-align:right;cursor:pointer; color: #ed3237;}
#topBtn {
	display: block;
	position: fixed;
	right: 6px;
	bottom: 100px;
	width: 40px;
	height: 35px;
	border-radius: 3px;
	cursor: pointer;
	background: rgba(0, 0, 0, 0.9);
	transform: rotate(90deg);
	border-radius: 50%;
	display: none;
}
#topBtn::before {
	content: '\00AB';
	font-size: 37px;
	line-height: 30px;
	color: #fff;
	display: block;
	text-align: center;
}
</style>


<!-- Modal -->
<div id="regLog" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subscriber Register/Login Form</h4>
      </div>
      <div class="modal-body">
        <div class="c-field" id="newReg">
			<form name="register" role="form" method="post" id="reused_form" action="">
				<p id="msg"></p>
				<span class="loader"><img src="http://www.loofre.co.in/images/loader.gif" /></span>
				<div class="form-group" id="couponCode">
					<label for="code">
						Enter Code:</label>
					<input type="text" class="form-control"
					id="code" name="couponCode" value="" required maxlength="20" data-minlength="6">
				</div>
				<div class="form-group r-field">
					<label for="name">
						Name:</label>
					<input type="name" class="form-control"
					id="name" name="name" value="" required maxlength="50">
				</div>
				<div class="form-group r-field">
					<label for="email">
						Email:</label>
					<input type="email" class="form-control"
					id="email" name="email" value="" required maxlength="50">
				</div>
				<div class="form-group r-field" id="btn">
					<label for="mobile">
						Mobile:</label>
					<input type="mobile" class="form-control"
					id="mobile" name="mobileNumber" value="" required maxlength="10">
				</div>
				<button type="submit" class="btn-lg btn-success btn-block r-field" id="sub_btn">Submit</button>
				<p id="alradyAccTxt">Are You Already Member ?</p>

			</form>
        </div>
        
        <!-- If Already have Active Account -->
        <div class="c-field" id="alradyAcc">
			<form name="register" role="form" method="post" id="Mregisterd_form" >
				<div class="form-group" >
					<label for="mobile">
						Enter Registered Mobile:</label><span id="backbtn">Back >></span>
					<input type="mobile" class="form-control"
					id="mobile" name="mobileNumber" value="" required maxlength="10">
				</div>
				<p id="msgM"></p>
				<span class="loader"><img src="http://www.loofre.co.in/images/loader.gif" /></span>
				<button type="submit" class="btn-lg btn-success btn-block" id="Msub_btn">Submit</button>
			</form>
        </div>
      </div>

    </div>

  </div>
</div>

<script>
jQuery(document).ready(function($){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
            $('#topBtn').fadeIn('slow');
        } else {
            $('#topBtn').fadeOut('slow');
        }
    });
    $('#topBtn').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });
});
</script>
