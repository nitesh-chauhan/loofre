<div class="container search-conent">
	<div class="col-lg-12 main-content" itemscope itemtype="http://schema.org/Restaurant">
    <div id="floating-panel">
      <input id="localityboxInput" type="text" value="" placeholder="Search Location">
		<!--<input id="localityboxInput" type="text" placeholder="Search.." value=""> -->
		<div id="localitylist">
		  <ul id="localityitem">	
		  </ul>
		</div>
      <!--<input id="submit" type="button" value="Search">-->
    </div>
    <div id="map"></div>

    <script>

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: 40.731, lng: -73.997}
        });
        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;
			geocodeLatLng(geocoder, map, infowindow);


        //$('document').ready(function(){
			//geocodeLatLng(geocoder, map, infowindow);
			
		//	});
      }

      function geocodeLatLng(geocoder, map, infowindow) { 
        //var latlng = {lat: parseFloat(28.5703), lng: parseFloat(77.3218)};
        var latlng = {lat: parseFloat(localStorage.getItem('Latitude')), lng: parseFloat(localStorage.getItem('Longitude'))};
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
				//alert(results[0].formatted_address);
				var formated_add = (results[0].formatted_address);
					for (var i = 0; i < results.length; i++) {
						//console.log(results[i]);
						if (results[i].types[0] === "locality") {
							var city = results[i].address_components[0].short_name;
							//var state = results[i].address_components[0].short_name;
							var cityStorage = localStorage.getItem('city');
							if(cityStorage){
								$("#cityfixed").html(cityStorage);
								}else{
								$("#cityfixed").html(city);
								localStorage.setItem("city", city);									
									}

						}
						if (results[i].types[2] === "sublocality_level_1") {
							var locality = results[i].address_components[0].long_name;
							var localityStorage = localStorage.getItem('locality');
							if((localityStorage) && (localityStorage!=locality)){
								$("#localityboxInput").val(localityStorage);
								}else{
								$("#localityboxInput").val(locality);
								localStorage.setItem("locality", locality);
									}
								
						}
					}
				    
				  //  var localityStorage = localStorage.getItem('locality');
				//			if(localityStorage){
				//				$("#localityboxInput").val(localityStorage);
				//				}else{
				//				$("#localityboxInput").val(formated_add);
				//				localStorage.setItem("locality", formated_add);	
				//				location.reload(true);								
				//					}
				    
              map.setZoom(11);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLpNCboKr1hGLZem--922yQ2E3BJmqRf0&callback=initMap">
    </script>		
	</div>
</div>

