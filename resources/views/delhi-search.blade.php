@extends('layouts.admin') @section('content')
<style>
    .supplierColor{
        border-color: #f10505; 
    } 
</style>

 <section class="content-header">
    <h1>Alliance Search Form</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Alliance</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Please select city Details Below</h3>
            
        </div>
        <!-- /.box-header --> 
       <form enctype="multipart/form-data" action="{{ url('delhi-search-result')}}" method="POST">
       {{csrf_field()}}
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"> 
                        <label for="exampleInputEmail1">select city Type<span class="supplierColor">*</span></label>
						<select class="form-control supplierColor" name="cititype" id="cititype" required>
                        <option value="">select city Name</option> 
						<option value="Delhi">Delhi</option> 
                      </select> 
                    </div>
                </div>
                <!-- /.col --> 
            <div class="row">
              
                <!-- /.col -->
                <div class="col-md-3">
                    <div class="box-footer">
                        <input type="submit" id="insertSupplier" class="btn btn-success btn-sm" value="Submit"/> 
                    </div>
                </div>                
                <div class="col-md-4"></div>
            </div> 
            
            
        </div> 
        </form>
    </div>
</section>
 
 
@endsection