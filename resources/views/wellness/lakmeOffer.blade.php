@extends('layouts.lakme-layout') @section('content')
<div class="agreement-main" id="signupdiv" style="display: block;">
	<div class="agreement-head">
		<h2><b>Lakme / MasterCard Program</b></h2>
		@if ($message = Session::get('success')) 
							<h1 style="background-color:green;color:#fff;font-size:26px;padding:5px;margin:5px 0px;" class="myhideDiv">{{ $message }} </h1>							 
							@endif
		 
	</div>
	<div style="clear:both;">	 
		<form method="post" action="{{ url('lakme-voucher-sms')}}" id="registration-form" 
		enctype="multipart/form-data" onsubmit="myFunction()">
		{{ csrf_field() }} 
			<div class="left  log-panel margin-rt30">
				<div class="reg-form" style="clear:both; margin:0 auto;width:70%;">
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<div class="clear-both" style="font-size:18px; padding:20px 0 0 0;
						margin-bottom: 10px; width:100%; color: #666; border-bottom: 2px solid #666;">
						Please Fill the form to get the Lakme offer Code.
					</div>
						<div class="clear-both" style=" text-align:center;"> 
							<div class="left col">
								<div class="left field-title">Name<font color="#FF6600">*</font></div>
								<div class="left field-content">
									<input name="name" id="organisation" value="" type="text" class="countrySet" required>
									<div id="organisation_error" class="error"></div>
								</div>
							</div> 
					
							<div class="left col">
								<div class="left field-title">Mobile*<font color="#FF6600">*</font></div>
								<div class="left field-content">
									<input name="mobile" maxlength="10" id="organisation" value="" type="text" class="countrySet" required>
									<div id="organisation_error" class="error"></div>
								</div>
							</div>
						</div>
				
						<div style="clear:both; text-align:center;">
							<div class="right field-content">
								<input style="float:right;" type="submit" name="insertSupplier" id="memberButtonsubmit"class="send-button" value="SUBMIT"/>
							</div>
						</div> 
					</div>
				<div class="clear-both" style="height:30px;"></div>
			</div>
		</form>
	</div> 
</div>
<script> 

function myFunction() {
  alert("Lakmi Code Will Be Send");
}
   setTimeout(function() {	  
    $('.myhideDiv').fadeOut('slow');
}, 20000); 

</script>
@endsection 
