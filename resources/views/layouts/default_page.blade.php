<!DOCTYPE html>
<html>
<head>
    @include('FrontWebPage.includes.head')
</head>
@extends('FrontWebPage.includes.nav_tabs')
@extends('FrontWebPage.includes.sidebarAccount')
<body id="bd">

	<div id="header_section">
		@include('FrontWebPage.includes.header_inner')
		@include('FrontWebPage.includes.search')

	</div>

    <div id="main" class="page_content">
		@yield('content')
    </div>

	<div id="footer_section" style="padding:20px 0px; background: #3c3c3c;">
		@include('FrontWebPage.includes.footer-inner')
	</div>

</body>
</html>


