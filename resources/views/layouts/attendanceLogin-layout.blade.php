<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="robots" content="noindex,follow"/>
    <script src="{{ asset('/frontEnd/homePage')}}/js/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/style.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/login.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/merchant.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/merchant_styles.css"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('/admin')}}/link/style_crm.css"/> 
	<title>Loofre Attendance Login</title> 
</head>
<body>
	<header>
		<div id="headerWrap">
			<div id="headerLeft">
				<h1 id="logo">
					<a href="https://www.loofre.com/">
					<img src="{{asset('/images/loofre10.png') }}"/>						 
					</a>
				</h1>
			</div><!-- #headerLeft -->
			<div id="headerRight">
				<div id="loginStatus" class="">
					 
				</div><!-- #loginStatus -->
				 
			</div><!-- #headerRight -->
		</div><!-- #headerWrap -->
	</header>	
	<div class="agreement-main formcontroller" style="clear:both; border-bottom:4px solid #EB2429; ">
		<div style=" margin-right:30px; line-height:31px; text-align:right;">
			<!---<a href="javascript:void(0);" id="showlogin" class="">Login</a>---> 
			<a href="javascript:void(0);" id="showsignup" class="active">Registration</a>
		</div>
	</div>
	 
 @yield('content') 



<style>
.li_3 li {float:left; width:33.33333%;}
</style>
<!--FOOTER STARTS-->
<div class="footer_main">
  <div class="top bggrey">
    <div class="container">
      <div class="master">
        <div class="row">
          <div class="footerLinks2"> 
          <a href="https://www.loofre.com/about-us/" class="fsmalllinks">About Us</a> 
          <a href="https://www.loofre.com/our-team/" class="fsmalllinks">Team</a> 				<!--	45	 --> 
          <a href="https://www.loofre.com/contact-us/" class="fsmalllinks last">Contact Us</a> 
          <a style="width: 80%;max-width: 41%;padding: 10px 9.5%;" class="width241" href="https://www.loofre.com/merchant/">Add Your Restaurant</a>
        </div>
          <div class="discoverdiv float_r">
            <h3>Discover great places to eat around you from 2468 registered restaurants</h3>
            <p></p>
            <div class="footer_social"> Join us 
                <a href="https://www.facebook.com/loofreonweb" target="_blank" class="fs_facebook"></a> 
                <a href="https://twitter.com/loofre" target="_blank" class="fs_twitter"></a> 
                <a href="https://plus.google.com/102325043925898684671/about" target="_blank" class="fs_gplus"></a> 
                <a href="https://www.youtube.com/channel/UCM55JsUBN2B1BLrJ15xsSjg" target="_blank" class="fs_youtube"></a> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="top copyright">
    <div class="container">
      <div class="master">
        <div class="copy_left">Copyright © 2015 Loofre, All rights reserved <a href="https://www.loofre.com/privacy-policy">Privacy Policy </a><span>/</span> 
        <a href="https://www.loofre.com/terms-of-use/">Terms of Use</a><span>/</span> 
        <a href="https://www.loofre.com/sitemap.xml">Sitemap</a><span>/</span> 
        <div class="copy_right">Designed &amp; Developed by : <a href="javascript:">K5 Brand Solutions</a></div>
      </div>
    </div>
  </div>
</div>
<!--FOOTER END-->
</div>

<!--MOBILE FOOTER STARTS-->
<div class="footer_main_mobile">
  <div class="top bggrey">
    <div class="container">
      <div class="master">
        <div class="row">
          <div class="discoverdiv">
            <h3>Discover great places to eat around you</h3>             
            <div class="footer_social"> <span>Join us</span>            
                <a href="https://www.facebook.com/LoOfRe" target="_blank" class="fs_facebook"></a> 
                <a href="https://twitter.com/loofre" target="_blank" class="fs_twitter"></a> 
                <a href="https://plus.google.com/102325043925898684671/about" target="_blank" class="fs_gplus"></a> 
                <a href="https://www.youtube.com/channel/UCM55JsUBN2B1BLrJ15xsSjg" target="_blank" class="fs_youtube"></a>            
            </div>
          </div>
          <div class="footerLinks2"> 
          <a href="https://www.loofre.com/about-us/" class="fsmalllinks">About Us</a> 
          <a href="https://www.loofre.com/our-team/" class="fsmalllinks">Team</a> 				<!--	45	 --> 
          <a href="https://www.loofre.com/contact-us/" class="fsmalllinks last">Contact Us</a> 
          <a class="fsmalllinks" href="">Add Your Restaurant</a>
        </div>
          <div class="copyrightLinks">
          <a href="https://www.loofre.com/privacy-policy">Privacy Policy </a><span>/</span> 
        <a href="https://www.loofre.com/terms-of-use/">Terms of Use</a><span>/</span> 
        <a href="https://www.loofre.com/sitemap.xml">Sitemap</a><span>/</span> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="top copyright">
    <div class="container">
      <div class="master">
        <div class="copy_left">Copyright © 2015 Loofre, All rights reserved</div>
        <div class="copy_right">Designed &amp; Developed by : <a rel="nofollow" href="https://www.loofre.com/merchant/#">K5 Brand Solutions</a></div>
      </div>
    </div>
  </div>
</div>
<!--MOBILE FOOTER End-->  
<!--Flexslider--> 
<style>
.showFancyBoxfWrap {position:fixed;	z-index:999999; top:0; left:0; width:100%; height:100%;
background:#000; opacity:0.7; } .fancyDiv {position:fixed; z-index:9999999; top:50%; left:50%;
margin-left:0; margin-top:0; } .closeFancy {position: relative; z-index: 999999999; float:right;
font: 600 16px/18px 'Open Sans', Arial, Helvetica, sans-serif; color:#fff; background:#ccc; width:
auto; height: auto; padding: 10px 15px; border-radius: 110%; margin: -20px; cursor:pointer; } 
.closeFancy:hover {color:#ff; background:#FF5336; opacity:1; } 
	
.header_popup_login_social {position:fixed; z-index:9999; left:50%; top:50%; width:240px; height:370px; margin:-185px 0 0 -120px; }
.socialMainDiv {float:left; width: 100%; margin:0 auto; padding:20px; border: 5px solid #ca292e; background:#fff; overflow:hidden;}
.socialMainDiv .caption {float:left; width: 100%; font:500 12px 'Open Sans'; color:#888; }
.socialMainDiv .row {float:left; width: 100%; position:relative; }
.socialMainDiv .row .lavel {float:left; width: 100%; font:500 14px/25px 'Open Sans'; color:#000; margin:0 0 20px 0;}
.socialMainDiv .row .text {float:left; padding:0 0 0 5px; margin:0 0 10px 0; color:#999999; font:500 14px 'Open Sans'; height:46px; border:1px solid #999999; -moz-border-radius:3px; -khtml-border-radius:3px; -webkit-border-radius:3px; border-radius:3px;}
.socialMainDiv .soical_submit {float:left; width: 100%; height:40px; font: 500 20px/40px 'Open Sans', Arial, Helvetica, sans-serif; color:#fff; padding: 0 0px; background:#ca292e !important; border-radius:0px; margin: 10px 10px 20px 0px; text-align:center; border:0 none; cursor:pointer; }
.socialMainDiv .focusVal {position: absolute; top: 15px; text-align: center; width: 100%; }
</style>  
</body>
</html>
