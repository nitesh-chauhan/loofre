
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" class=" js csstransforms csstransforms3d csstransitions gecko linux js js_active  vc_desktop  vc_transform  vc_transform  vc_transform " lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="alternate" type="application/rss+xml" title="I Rock India RSS Feed" href="https://www.loofre.com/irock/feed">
    <link rel="alternate" type="application/atom+xml" title="I Rock India Atom Feed" href="https://www.loofre.com/irock/feed/atom">

    <link rel="stylesheet" href="{{ asset('/frontEnd/homePage')}}/css/css.css">
    <link rel="stylesheet" href="{{ asset('/frontEnd/homePage')}}/css/css_002.css">
    <link rel="stylesheet" href="{{ asset('/admin/bower_components/bootstrap/dist')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/admin/bower_components/font-awesome')}}/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('/admin/bower_components/Ionicons')}}/css/ionicons.min.css"> 
	<link rel="stylesheet" href="{{ asset('/frontEnd/')}}/css/style.css">

    <script src="{{ asset('/frontEnd/homePage')}}/js/jquery.min.js"></script>
    <script src="{{ asset('/frontEnd')}}/js/app-custom.js"></script>
    <script src="{{ asset('/frontEnd')}}/js/search.js"></script>
    <script src="{{ asset('/frontEnd/homePage')}}/js/bootstrap.min.js"></script>
    

    <meta name="description" content="Interviews of Best Female Djs, Male Djs, Bands, Chefs, Food Bloggers, Party Coverage, International DJ, Event News and All about parties.">
    <meta name="keywords" content="Interviews of Best Female Djs, Male Djs, Bands, Chefs, Food Bloggers, Party Coverage, International DJ, Event News and All about parties.">

    <!-- <link rel="canonical" href="https://www.loofre.com/I Rock India/" /> -->
    <link rel="publisher" href="https://plus.google.com/102325043925898684671/about">
    <meta name="robots" content="index, follow">

    <meta property="og:title" content="">
    <meta property="og:url" content="https://www.loofre.com/irock/">
    <meta property="og:site_name" content="Loofre">
    <meta property="og:image" content="https://www.loofre.com/irock/wp-content/themes/fashery/assets/images/LOGO_india2.jpg">
    <meta property="og:description" content="Interviews of Best Female Djs, Male Djs, Bands, Chefs, Food Bloggers, Party Coverage, International DJ, Event News and All about parties.">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@loofre">
    <meta name="twitter:url" content="https://www.loofre.com/irock/">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="Interviews of Best Female Djs, Male Djs, Bands, Chefs, Food Bloggers, Party Coverage, International DJ, Event News and All about parties.">
    <meta name="twitter:image" content="https://www.loofre.com/irock/wp-content/themes/fashery/assets/images/LOGO_india2.jpg">
    <link rel="shortcut icon" href="https://www.loofre.com/favicon.ico" type="image/x-icon">

    <!-- This site is optimized with the Yoast SEO plugin v5.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Best Chef, Bands, DJs, Female DJ, Male DJ, Fashion, singer &amp; Comedians</title>
    <meta name="description" content="Find National and International Chef, Bands, DJs, Female DJ, Male DJ &amp; Comedians, Tattoo, Singers,Fashion, Bloggers on www.loofre.com">
    <link rel="canonical" href="https://www.loofre.com/irock/">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Best Chef, Bands, DJs, Female DJ, Male DJ, Fashion, singer &amp; Comedians">
    <meta property="og:description" content="Find National and International Chef, Bands, DJs, Female DJ, Male DJ &amp; Comedians, Tattoo, Singers,Fashion, Bloggers on www.loofre.com">
    <meta property="og:url" content="https://www.loofre.com/irock/">
    <meta property="og:site_name" content="I Rock India">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:description" content="Find National and International Chef, Bands, DJs, Female DJ, Male DJ &amp; Comedians, Tattoo, Singers,Fashion, Bloggers on www.loofre.com">
    <meta name="twitter:title" content="Best Chef, Bands, DJs, Female DJ, Male DJ, Fashion, singer &amp; Comedians">
    <script id="facebook-jssdk" src="{{ asset('/frontEnd/homePage')}}/js/sdk.js"></script>
    <script async="" src="{{ asset('/frontEnd/homePage')}}/js/analytics.js"></script>
    <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"https:\/\/www.loofre.com\/irock","name":"I Rock India","potentialAction":{"@type":"SearchAction","target":"https:\/\/www.loofre.com\/irock?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/jquery.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/js.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/royal_jquery.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/royal_custom.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/js_composer_front.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd/homePage')}}/js/royal_masterslider.js"></script>
    <script type="text/javascript" src="{{ asset('/frontEnd')}}/js/api-home.js"></script>


    <link rel="stylesheet" id="royal_style-css" href="{{ asset('/frontEnd/homePage')}}/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="royal_shortcodes-css" href="{{ asset('/frontEnd/homePage')}}/css/royal_shortcodes.css" type="text/css" media="all">
    <link rel="stylesheet" id="royal_responsive-css" href="{{ asset('/frontEnd/homePage')}}/css/royal_responsive.css" type="text/css" media="all">
    <link rel="stylesheet" id="mmm_mega_main_menu-css" href="{{ asset('/frontEnd/homePage')}}/css/cache.css" type="text/css" media="all">
    <link rel="stylesheet" id="js_composer_front-css" href="{{ asset('/frontEnd/homePage')}}/css/js_composer.css" type="text/css" media="all">
    <link rel="stylesheet" id="" href="{{ asset('/frontEnd/homePage')}}/css/custom.css" type="text/css" media="all">

    <link rel="https://api.w.org/" href="https://www.loofre.com/irock/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.loofre.com/irock/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.loofre.com/irock/wp-includes/wlwmanifest.xml">
    <link rel="shortlink" href="https://www.loofre.com/">
    <link rel="alternate" type="application/json+oembed" href="https://www.loofre.com/irock/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.loofre.com%2Firock%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://www.loofre.com/irock/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.loofre.com%2Firock%2F&amp;format=xml">
    <script type="text/javascript">
        try {
            var d = new Date();
            jQuery.cookie('cbg_tz', d.getTimezoneOffset(), {
                path: '/',
                expires: 366
            });
        } catch (err) {}
    </script>
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.loofre.com/irock/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
    <noscript>
        <style type="text/css">
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
</head>


@yield('content')
@extends('FrontWebPage.includes.nav_tabs')
@extends('FrontWebPage.includes.sidebarAccount')
 


