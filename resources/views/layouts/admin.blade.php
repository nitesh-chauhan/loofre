<!DOCTYPE html>
<html>
<head>
  <meta name="robots" content="noindex"/>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('/admin')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="{{ asset('/admin')}}/bower_components/bootstrap/dist/css/css.css">  
 <script src="{{ asset('/admin')}}/bower_components/bootstrap/dist/js/jquery-1.10.2.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">Loofre Administrator</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Loofre Administrator</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">         
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">              
              <span class="hidden-xs">Hello {{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header"> 
                <p>
                  {{ Auth::user()->name }}
                  <small>Member since Nov. 2018</small>
                </p> 
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
               
              </li>
              <!-- Menu Body -->              
              <!-- Menu Footer-->
              
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('/images/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Welcome :{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> 
      <!-- search form -->      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('dashboard') }}"><i class="fa fa-circle-o"></i> Dashboard</a></li>          
          </ul>
        </li> 
                        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Supplier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('supplier-list') }}"><i class="fa fa-circle-o"></i> Supplier List</a></li>
			<li><a href="{{ route('supplier-list-card') }}"><i class="fa fa-circle-o"></i> Card Supplier List</a></li>   
            <li><a href="{{ route('add-supplier') }}"><i class="fa fa-circle-o"></i> Add Supplier</a></li> 
           			
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>App Coupon </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('app-coupon-upload') }}"><i class="fa fa-circle-o"></i> App Coupon List </a></li>   
            <li><a href="{{ route('app-coupon') }}"><i class="fa fa-circle-o"></i> Add App Coupon from CSV</a></li>
			<li><a href="{{ route('app-coupon-from') }}"><i class="fa fa-circle-o"></i> Add App Coupon</a></li>     
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>App User </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('user-list') }}"><i class="fa fa-circle-o"></i> App User List</a></li>             
          </ul>
        </li> 	
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Brand</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('add-brand') }}"><i class="fa fa-circle-o"></i>Add Brand</a></li>
            <li><a href="{{ route('brand-list') }}"><i class="fa fa-circle-o"></i>Brand List</a></li>
          </ul>
        </li>
        
         <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Restaurant</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li><a href="{{ route('add-vender') }}"><i class="fa fa-circle-o"></i>Add Vender</a></li>			 
			<li><a href="{{ route('vender-list') }}"><i class="fa fa-circle-o"></i>Vender List</a></li>			 
          </ul>
         </li>

		 <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Trending Brands</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li><a href="{{ route('live-restaurant-for-home') }}"><i class="fa fa-circle-o"></i>Trending Brands List </a></li>
			<li><a href="{{ route('add-trending-brands') }}"><i class="fa fa-circle-o"></i>Add Trending Brands</a></li>			 
		  </ul>
         </li>
		 
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-leaf"></i> <span>Wellness</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> 
            <li><a href="{{ route('spa-add-vender') }}"><i class="fa fa-circle-o"></i>Add Spa Vender</a></li>			 
			<li><a href="{{ route('spa-vender-list') }}"><i class="fa fa-circle-o"></i>Spa Vender List</a></li>			 
          </ul>
         </li>
		
		<li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-wrench"></i> <span>Settings</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="{{ route('add-menu') }}"><i class="fa fa-circle-o"></i>Add Menu</a></li>
				<li><a href="{{ route('menu-list') }}"><i class="fa fa-circle-o"></i>Menu List</a></li>
				 				 
			  </ul>
		</li>
		
		<li class="treeview">
			  <a href="#">
				<i class="fa fa-book"></i> <span>Booking</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{ route('dining-booking-list') }}"><i class="fa fa-circle-o"></i>Dining Booking List</a></li>
				<li><a href="{{ route('buffet-booking-list') }}"><i class="fa fa-circle-o"></i>Buffet Booking List</a></li>
			  <li><a href="{{ route('spa-booking-list') }}"><i class="fa fa-circle-o"></i>Spa Booking List</a></li>
			  </ul>
		</li> 
		 <li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-star"></i> <span>Feedback</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{ route('feedback-list') }}"><i class="fa fa-circle-o"></i>Feedback List</a></li> 
			  </ul>
		</li> 


		<li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-star"></i> <span>WebSite link (www.loofre.com)</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{ route('old-crm') }}"><i class="fa fa-circle-o"></i>WebSite List</a></li>
				<li><a href="{{ route('old-buffet') }}"><i class="fa fa-circle-o"></i>Buffet List</a></li> 
			  </ul>
		</li>

        <li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-star"></i> <span>Payment Log</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{ route('payment-log') }}"><i class="fa fa-circle-o"></i>Payment Log List</a></li> 
			  </ul>
		</li> 
		
		
	 	 <li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-star"></i> <span>IROCK Reports</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{route('irock-register')}}"><i class="fa fa-circle-o"></i>IROCK List</a></li> 
				<li><a href="{{route('cloub-register')}}"><i class="fa fa-circle-o"></i>club List</a></li> 
			  </ul>
		</li> 
	 	 <li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>Notification</span> 
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{route('notification-list')}}"><i class="fa fa-circle-o"></i>Notification List</a></li> 
				<li><a href="{{route('add-notification')}}"><i class="fa fa-circle-o"></i>Add Notification</a></li> 
			  </ul>
		 </li>
        <li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>Premium Dining</span> 
			  </a> 
			  <ul class="treeview-menu"> 
				<li><a href="{{route('premium-dining-list')}}"><i class="fa fa-circle-o"></i>Premium Dining List</a></li> 
				<li><a href="{{route('add-premium-dining')}}"><i class="fa fa-circle-o"></i>Add Premium Dining</a></li>
			  </ul>
		 </li>
		 
		   <li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>Voucher</span> 
			  </a> 
			  <ul class="treeview-menu"> 
				<li><a href="{{route('voucher-list')}}"><i class="fa fa-circle-o"></i>Voucher List</a></li> 
				<li><a href="{{route('add-voucher')}}"><i class="fa fa-circle-o"></i>Add Voucher </a></li>
			  </ul>
		 </li>


		 
		<li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>Daily Alliance</span> 
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{route('pending-alliance-list')}}"><i class="fa fa-circle-o"></i>Pending Alliance List</a></li> 
				<li><a href="{{route('add-daily-alliance')}}"><i class="fa fa-circle-o"></i>Add Alliance</a></li> 
			  </ul>
		 </li> 
		 
		 <li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>Astrologer</span> 
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{route('astrologer-list')}}"><i class="fa fa-circle-o"></i>Astrologer List</a></li> 
				  
			  </ul>
		 </li> 
		 
		 
		 
		 
		 
         <!-------<li class="treeview">
			  <a href="#">
				<span class="glyphicon glyphicon-flag"></span> <span>New Year</span> 
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{route('bank-list')}}"><i class="fa fa-circle-o"></i>New Year</a></li> 
				 
			  </ul>
		 </li>	------------->
		 
   <!------ <li class="treeview">
			  <a href="#">
				<i class="glyphicon glyphicon-star"></i> <span>Alliance Search Strikers</span>
				<span class="pull-right-container">
				  <i class="fa fa-angle-left pull-right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu"> 
				<li><a href="{{ route('alliance-search') }}"><i class="fa fa-circle-o"></i>Alliance Search</a></li>
                <li><a href="{{ route('lucknow-search') }}"><i class="fa fa-circle-o"></i>Lucknow Search</a></li>	
				<li><a href="{{ route('kolkata-search') }}"><i class="fa fa-circle-o"></i>kolkata Search</a></li>	
				<li><a href="{{ route('delhi-search') }}"><i class="fa fa-circle-o"></i>Delhi Search</a></li>	
				
			  </ul>
		</li>----------------->  
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <!-- Main content --> 
     @yield('content') 
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018-2019 </strong>Loofre  All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3  remove-->
  <script src="{{ asset('/admin')}}/bower_components/jquery/dist/jquery.min.js"></script>   
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('/admin')}}/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/admin')}}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="{{ asset('/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<!-- Morris.js charts -->
<script src="{{ asset('/admin')}}/bower_components/raphael/raphael.min.js"></script>
<script src="{{ asset('/admin')}}/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{ asset('/admin')}}/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{ asset('/admin')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/admin')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('/admin')}}/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ asset('/admin')}}/bower_components/moment/min/moment.min.js"></script>
<script src="{{ asset('/admin')}}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{ asset('/public/admin')}}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/admin')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{ asset('/admin')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{ asset('/admin')}}/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/admin')}}/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('/admin')}}/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('/admin')}}/dist/js/demo.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
 
 

</body>
</html>
